# **Software Design & Implementation Assignment - Farming Simulator** #

## **Project** 
A Team Project to make a game which is a semi-realistic farming simulator,developed from the stage to designing all the way to the stage of implementing and testing the game.

## **Team** ##
Team of 5, including me. Consisted of Lead Project Manager, Lead QA Manager's, Lead Programmer and Lead Designer.
I acted as the Lead Programmer, and was responsible programming most of the project myself aswell as for designing the Use Case diagrams and assigning classes to program for others.
I also participated in testing my own implemented code.

## **My Work** ##
The Classes I programmed:

* Field
* NextTurn
* SelectedField
* Program
* Weather

All of the team contributed to adding in the UI Code ( for buttons, and display of text )