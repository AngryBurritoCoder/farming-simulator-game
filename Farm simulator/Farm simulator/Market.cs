﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Created by Franz Pascual
//Version 1.0

namespace Farm_simulator
{
    class Market
    {
        #region Attributes

        private static float fruitSeedPrice;
        private static float vegSeedPrice;
        private static float cerealSeedPrice;
        private static float pesticidePrice;
        private static float fertiliserPrice;
        private static float vegSellPrice;
        private static float fruitSellPrice;
        private static float cerealSellPrice;
        private static float moneySpent;
        private static float fieldPrice;

        

        

        private Stats stats = new Stats();
        //private Bank bank; if bank exists
        //private Storage storage; if storage exists

        #endregion

        #region Encapsulation


        public float CerealSellPrice
        {
            get { return cerealSellPrice; }
            set { cerealSellPrice = value; }
        }

        public float FruitSellPrice
        {
            get { return fruitSellPrice; }
            set { fruitSellPrice = value; }
        }

        public float VegSellPrice
        {
            get { return vegSellPrice; }
            set { vegSellPrice = value; }
        }
        

        public float FertiliserPrice
        {
            get { return fertiliserPrice; }
            set { fertiliserPrice = value; }
        }


        public float PesticidePrice
        {
            get { return pesticidePrice; }
            set { pesticidePrice = value; }
        }


        public float VegSeedPrice
        {
            get { return vegSeedPrice; }
            set { vegSeedPrice = value; }
        }
        

        public float CerealSeedPrice
        {
            get { return cerealSeedPrice; }
            set { cerealSeedPrice = value; }
        }


        public float FruitSeedPrice
        {
            get { return fruitSeedPrice; }
            set { fruitSeedPrice = value; }
        }

        public float MoneySpent
        {
            get { return Market.moneySpent; }
            set { Market.moneySpent = value; }
        }
        
        public float FieldPrice
        {
            get { return Market.fieldPrice; }
            set { Market.fieldPrice = value; }
        }




        #endregion

        #region Methods
        //**
        /// <summary>
        /// A function to sell stored crops to the market (**method not used. Created for back-up purposes)
        /// </summary>
        /// <param name="crop"></param>
        /// determine which crop to be sold (Please enter one of the following without quotation marks: "Fruit", "Veg" or "Cereal")
        /// <param name="quality"></param>
        /// Enter the quality of the crop
        /// <param name="amount"></param>
        /// enter the amount to be sold
        public void SellCrop(string crop, float quality, int amount)
        {
            float price = 0;
            switch (crop)
            { 
                case "Veg":                    
                    price = (vegSellPrice * quality) * amount;                   
                    stats.UpdateRevenue(price);
                    break;
                case "Fruit":
                    //if storage veg exists and > amount then
                    price = (fruitSellPrice * quality) * amount;
                    //bank currentBalance += price
                    //storage fruit -= amount
                    stats.UpdateRevenue(price);
                    break;
                case "Cereal":
                    //if storage veg exists and > amount then
                    price = (cerealSellPrice * quality) * amount;
                    //bank currentBalance += price
                    //storage cereal -= amount
                    stats.UpdateRevenue(price);
                    break;
                default:
                    break;            
            }
        }
        

        public void BuyFertiliser(int amount)
        {
            //if bank current balance > fertiliserPrice * amount then
            moneySpent = fertiliserPrice * amount;
            //bank current balance -= moneySpent
            stats.AddSpecialItem("Fertiliser", amount);
            stats.UpdateMoneySpent(moneySpent);
        }
        public void BuyPesticide(int amount)
        {
            //if bank current balance > pesticidePrice * amount then
            moneySpent = pesticidePrice * amount;
            //bank current balance -= moneySpent
            stats.AddSpecialItem("Pesticide", amount);
            stats.UpdateMoneySpent(moneySpent);
        }
        public void BuySeeds(string seed, int amount)
        {
            
            switch (seed)
            {
                case "Fruit":
                    //if bank current balance > (fruitSeedPrice * amount) then
                    moneySpent = fruitSeedPrice * amount;
                    //bank current balance -= moneySpent
                    stats.AddSeed("Fruit", amount);
                    stats.UpdateMoneySpent(moneySpent);
                    break;
                case "Veg":
                    //if bank current balance > (vegSeedPrice * amount) then
                    moneySpent = vegSeedPrice * amount;
                    //bank current balance -= moneySpent
                    stats.AddSeed("Veg", amount);
                    stats.UpdateMoneySpent(moneySpent);
                    break;
                case "Cereal":
                    //if bank current balance > (cerealSeedPrice * amount) then
                    moneySpent = cerealSeedPrice * amount;
                    //bank current balance -= moneySpent
                    stats.AddSeed("Cereal", amount);
                    stats.UpdateMoneySpent(moneySpent);
                    break;
                default:
                    break;
            }
            
        }
        /// <summary>
        /// Randomise the number through random number generator class. The randomed number is based on the original price of every buyable instances on the game including field. After the randoming it updates the prices of the buyable items
        /// </summary>
        public void UpdatePrices()
        {
            Random random = new Random();//random instance created
            float originialPriceFruitSeed = 6500;
            float originialPriceVegSeed = 6000;
            float originialPriceCerealSeed = 5000;
            float originialPriceFruitSell = 60;
            float originialPriceVegSell = 50;
            float originialPriceCerealSell = 40;
            float originialPriceFertiliser = 3500;
            float originialPricePesticide = 10000;
            float originalPriceField = 100000;
            int randomPercent = 25;//value to be used for dividing the original price and create a min, max for randomising the original price.

            fruitSeedPrice = random.Next(Convert.ToInt32(originialPriceFruitSeed - (originialPriceFruitSeed / randomPercent)), Convert.ToInt32(originialPriceFruitSeed + (originialPriceFruitSeed / randomPercent)));
            vegSeedPrice = random.Next(Convert.ToInt32(originialPriceVegSeed - (originialPriceVegSeed / randomPercent)), Convert.ToInt32(originialPriceVegSeed + (originialPriceVegSeed / randomPercent)));
            cerealSeedPrice = random.Next(Convert.ToInt32(originialPriceCerealSeed - (originialPriceCerealSeed / randomPercent)), Convert.ToInt32(originialPriceCerealSeed + (originialPriceCerealSeed / randomPercent)));
            fruitSellPrice = random.Next(Convert.ToInt32(originialPriceFruitSell - (originialPriceFruitSell / randomPercent)), Convert.ToInt32(originialPriceFruitSell + (originialPriceFruitSell / randomPercent)));
            vegSellPrice = random.Next(Convert.ToInt32(originialPriceVegSell - (originialPriceVegSell / randomPercent)), Convert.ToInt32(originialPriceVegSell + (originialPriceVegSell / randomPercent)));
            cerealSellPrice = random.Next(Convert.ToInt32(originialPriceCerealSell - (originialPriceCerealSell / randomPercent)), Convert.ToInt32(originialPriceCerealSell + (originialPriceCerealSell / randomPercent)));
            fertiliserPrice = random.Next(Convert.ToInt32(originialPriceFertiliser - (originialPriceFertiliser / randomPercent)), Convert.ToInt32(originialPriceFertiliser + (originialPriceFertiliser / randomPercent)));
            pesticidePrice = random.Next(Convert.ToInt32(originialPricePesticide - (originialPricePesticide / randomPercent)), Convert.ToInt32(originialPricePesticide + (originialPricePesticide / randomPercent)));
            pesticidePrice = random.Next(Convert.ToInt32(originialPricePesticide - (originialPricePesticide / randomPercent)), Convert.ToInt32(originialPricePesticide + (originialPricePesticide / randomPercent)));
            fieldPrice = random.Next(Convert.ToInt32(originalPriceField - (originalPriceField / randomPercent)), Convert.ToInt32(originalPriceField + (originalPriceField / randomPercent)));
            
        }

        #endregion

    }
}
