﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Main_Menu : Form
    {

        private Player playerClass = new Player();
        private NextTurn nextTurnClass = new NextTurn();
        private Weather weatherClass = new Weather();
        private Bank bankClass = new Bank();
        private Storage_Menu storageMenu;
        private Take_Loan_Menu loanMenu;
        private SelectedField fieldID = new SelectedField();
        private Market market = new Market();

        public Main_Menu()
        {
            InitializeComponent();
            storageMenu = new Storage_Menu(this);
            loanMenu = new Take_Loan_Menu(this);
            UpdateLabels();
            market.UpdatePrices();
        }

        public void UpdateLabels()
        {
            label_weekNumber.Text = nextTurnClass.CurrentWeek.ToString() ;
            label_PlayerNameDisplay.Text = playerClass.PlayerName;
            label_FarmNameDisplay.Text = playerClass.FarmName;
            label_weekNumber.Text = nextTurnClass.CurrentWeek.ToString();
            label_weatherStatus.Text = weatherClass.WeatherType;
            label_seasonStatus.Text = nextTurnClass.CurrentSeason;
            label_moneyStatus.Text = "£" + bankClass.CurrentBalance.ToString();
            label_DebtDisplay.Text = "£" + bankClass.Loan.ToString();
            label_YearDisplay.Text = nextTurnClass.CurrentYear.ToString();

            #region Checking which field is bought
            //NEEDS CHANGING TO BETTER METHOD, was in a rush so used a very long winded and bad way of checking for which fields are bought
            if (Field.fields.Count == 2)
            {
                button_field3.Enabled = false;
                button_field4.Enabled = false;
                button_field5.Enabled = false;
                button_field6.Enabled = false;
            }

            if (Field.fields.Count == 3)
            {
                button_field3.Enabled = true;
                button_field4.Enabled = false;
                button_field5.Enabled = false;
                button_field6.Enabled = false;
            }

            if (Field.fields.Count == 4)
            {
                button_field3.Enabled = true;
                button_field4.Enabled = true;
                button_field5.Enabled = false;
                button_field6.Enabled = false;
            }

            if (Field.fields.Count == 5)
            {
                button_field3.Enabled = true;
                button_field4.Enabled = true;
                button_field5.Enabled = true;
                button_field6.Enabled = false;
            }

            if (Field.fields.Count == 6)
            {
                button_field3.Enabled = true;
                button_field4.Enabled = true;
                button_field5.Enabled = true;
                button_field6.Enabled = true;
            }
            #endregion

            #region Change colour based on field current statuses
            int n = -1;

            foreach (Field element in Field.fields)
            {
                n += 1;

                if (element.IsFieldInfected)
                {
                    switch (n)
                    {
                        case 0:
                            button_field1.BackColor = Color.Red;
                            break;
                        case 1:
                            button_field2.BackColor = Color.Red;
                            break;
                        case 2:
                            button_field3.BackColor = Color.Red;
                            break;
                        case 3:
                            button_field4.BackColor = Color.Red;
                            break;
                        case 4:
                            button_field5.BackColor = Color.Red;
                            break;
                        case 5:
                            button_field6.BackColor = Color.Red;
                            break;
                    }
                }

                if (!element.IsFieldInfected && !element.IsFieldCropFullyGrown && !element.IsFieldEmpty)
                {
                    switch (n)
                    {
                        case 0:
                            button_field1.BackColor = Color.Green;
                            break;
                        case 1:
                            button_field2.BackColor = Color.Green;
                            break;
                        case 2:
                            button_field3.BackColor = Color.Green;
                            break;
                        case 3:
                            button_field4.BackColor = Color.Green;
                            break;
                        case 4:
                            button_field5.BackColor = Color.Green;
                            break;
                        case 5:
                            button_field6.BackColor = Color.Green;
                            break;
                    }
                }

                if (!element.IsFieldInfected && element.IsFieldCropFullyGrown)
                {
                    switch (n)
                    {
                        case 0:
                            button_field1.BackColor = Color.Gold;
                            break;
                        case 1:
                            button_field2.BackColor = Color.Gold;
                            break;
                        case 2:
                            button_field3.BackColor = Color.Gold;
                            break;
                        case 3:
                            button_field4.BackColor = Color.Gold;
                            break;
                        case 4:
                            button_field5.BackColor = Color.Gold;
                            break;
                        case 5:
                            button_field6.BackColor = Color.Gold;
                            break;
                    }
                }

                if (element.IsFieldEmpty)
                {
                    switch (n)
                    {
                        case 0:
                            button_field1.BackColor = Color.SaddleBrown;
                            break;
                        case 1:
                            button_field2.BackColor = Color.SaddleBrown;
                            break;
                        case 2:
                            button_field3.BackColor = Color.SaddleBrown;
                            break;
                        case 3:
                            button_field4.BackColor = Color.SaddleBrown;
                            break;
                        case 4:
                            button_field5.BackColor = Color.SaddleBrown;
                            break;
                        case 5:
                            button_field6.BackColor = Color.SaddleBrown;
                            break;
                    }
                }
            }
            #endregion

        }

        private void sellCropsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Market_menu(0, this).ShowDialog();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            new Quit_Menu().ShowDialog();
        }

        private void Main_Menu_Load(object sender, EventArgs e)
        {

        }

        private void buySeedsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Market_menu(1, this).ShowDialog();    
        }

        private void takeLoanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loanMenu.ShowDialog();
        }

        private void button_Field_Test_Click(object sender, EventArgs e)
        {
            new Field_Information_Menu().ShowDialog();
        }

        private void storageStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            storageMenu.UpdateText();
            storageMenu.ShowDialog();
        }

        private void toolStripButton_stats_Click(object sender, EventArgs e)
        {
            new Stats_Menu().ShowDialog();
        }

        private void button_field1_Click(object sender, EventArgs e)
        {
            fieldID.FieldID = 0;
            Field_Information_Menu.FieldIDShown = 1.ToString();
            new Field_Information_Menu().ShowDialog();
        }

        private void toolStripButton_save_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Game Saved");
        }

        private void toolStripSplitButton_Market_ButtonClick(object sender, EventArgs e)
        {
            new Market_menu(0, this).ShowDialog();
        }

        private void button_NextTurn_Click(object sender, EventArgs e)
        {
            nextTurnClass.UpdateTurn();
            market.UpdatePrices();
            UpdateLabels();
            market.UpdatePrices();
        }

        private void label_seasonStatus_Click(object sender, EventArgs e)
        {

        }

        private void label_YearLabel_Click(object sender, EventArgs e)
        {

        }

        private void label_YearDisplay_Click(object sender, EventArgs e)
        {

        }

        private void label_weekNumber_Click(object sender, EventArgs e)
        {

        }

        private void label_weatherStatus_Click(object sender, EventArgs e)
        {

        }

        private void specialItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Market_menu(1, this, 2).ShowDialog();
        }

        private void toolStripSplitButton_Storage_ButtonClick(object sender, EventArgs e)
        {
            storageMenu.UpdateText();
            storageMenu.ShowDialog();
        }

        private void destroyCropToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Buy_Storage_Menu(storageMenu).ShowDialog();
        }

        private void button_field2_Click(object sender, EventArgs e)
        {
            fieldID.FieldID = 1;
            Field_Information_Menu.FieldIDShown = 2.ToString();
            new Field_Information_Menu().ShowDialog();
        }

        private void button_field3_Click(object sender, EventArgs e)
        {
            fieldID.FieldID = 2;
            Field_Information_Menu.FieldIDShown = 3.ToString();
            new Field_Information_Menu().ShowDialog();
        }

        private void button_field4_Click(object sender, EventArgs e)
        {
            fieldID.FieldID = 3;
            Field_Information_Menu.FieldIDShown = 4.ToString();
            new Field_Information_Menu().ShowDialog();
        }

        private void button_field5_Click(object sender, EventArgs e)
        {
            fieldID.FieldID = 4;
            Field_Information_Menu.FieldIDShown = 5.ToString();
            new Field_Information_Menu().ShowDialog();
        }

        private void button_field6_Click(object sender, EventArgs e)
        {
            fieldID.FieldID = 5;
            Field_Information_Menu.FieldIDShown = 6.ToString();
            new Field_Information_Menu().ShowDialog();
        }

        private void toolStripSplitButton_bank_ButtonClick(object sender, EventArgs e)
        {

        }

        private void payDebtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bankClass.Loan > 0)
            {
                if (bankClass.CurrentBalance >= bankClass.Loan)
                {
                    bankClass.PayLoan();
                    UpdateLabels();
                    loanMenu.UpdateText();
                }
            }
        }

        private void buyNewFieldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Market_menu(1, this, 4).ShowDialog();
        }
    }
}
