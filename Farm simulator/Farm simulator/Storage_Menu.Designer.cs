﻿namespace Farm_simulator
{
    partial class Storage_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Storage_Menu));
            this.label_vegStorage = new System.Windows.Forms.Label();
            this.label_fruitStorage = new System.Windows.Forms.Label();
            this.label_cerealStorage = new System.Windows.Forms.Label();
            this.button_buyStorage = new System.Windows.Forms.Button();
            this.label_VegStored = new System.Windows.Forms.Label();
            this.label_FruitStored = new System.Windows.Forms.Label();
            this.label_CerealStored = new System.Windows.Forms.Label();
            this.label_MaxVegLimit = new System.Windows.Forms.Label();
            this.label_MaxFruitLimit = new System.Windows.Forms.Label();
            this.label_MaxCerealLimit = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label_LimitInfo = new System.Windows.Forms.Label();
            this.label_StoredCurrentInfo = new System.Windows.Forms.Label();
            this.label_StorageType = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_vegStorage
            // 
            this.label_vegStorage.AutoSize = true;
            this.label_vegStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_vegStorage.Location = new System.Drawing.Point(12, 45);
            this.label_vegStorage.Name = "label_vegStorage";
            this.label_vegStorage.Size = new System.Drawing.Size(130, 17);
            this.label_vegStorage.TabIndex = 0;
            this.label_vegStorage.Text = "Vegetable Storage:";
            // 
            // label_fruitStorage
            // 
            this.label_fruitStorage.AutoSize = true;
            this.label_fruitStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fruitStorage.Location = new System.Drawing.Point(48, 78);
            this.label_fruitStorage.Name = "label_fruitStorage";
            this.label_fruitStorage.Size = new System.Drawing.Size(94, 17);
            this.label_fruitStorage.TabIndex = 1;
            this.label_fruitStorage.Text = "Fruit Storage:";
            // 
            // label_cerealStorage
            // 
            this.label_cerealStorage.AutoSize = true;
            this.label_cerealStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cerealStorage.Location = new System.Drawing.Point(35, 113);
            this.label_cerealStorage.Name = "label_cerealStorage";
            this.label_cerealStorage.Size = new System.Drawing.Size(107, 17);
            this.label_cerealStorage.TabIndex = 2;
            this.label_cerealStorage.Text = "Cereal Storage:";
            // 
            // button_buyStorage
            // 
            this.button_buyStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_buyStorage.Location = new System.Drawing.Point(12, 150);
            this.button_buyStorage.Name = "button_buyStorage";
            this.button_buyStorage.Size = new System.Drawing.Size(300, 48);
            this.button_buyStorage.TabIndex = 4;
            this.button_buyStorage.Text = "Buy Storage Space";
            this.button_buyStorage.UseVisualStyleBackColor = true;
            this.button_buyStorage.Click += new System.EventHandler(this.button_buyStorage_Click);
            // 
            // label_VegStored
            // 
            this.label_VegStored.AutoSize = true;
            this.label_VegStored.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_VegStored.Location = new System.Drawing.Point(148, 45);
            this.label_VegStored.Name = "label_VegStored";
            this.label_VegStored.Size = new System.Drawing.Size(79, 17);
            this.label_VegStored.TabIndex = 5;
            this.label_VegStored.Text = "Veg Stored";
            this.label_VegStored.Click += new System.EventHandler(this.label_VegStored_Click);
            // 
            // label_FruitStored
            // 
            this.label_FruitStored.AutoSize = true;
            this.label_FruitStored.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FruitStored.Location = new System.Drawing.Point(148, 78);
            this.label_FruitStored.Name = "label_FruitStored";
            this.label_FruitStored.Size = new System.Drawing.Size(82, 17);
            this.label_FruitStored.TabIndex = 6;
            this.label_FruitStored.Text = "Fruit Stored";
            this.label_FruitStored.Click += new System.EventHandler(this.label_FruitStored_Click);
            // 
            // label_CerealStored
            // 
            this.label_CerealStored.AutoSize = true;
            this.label_CerealStored.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CerealStored.Location = new System.Drawing.Point(148, 113);
            this.label_CerealStored.Name = "label_CerealStored";
            this.label_CerealStored.Size = new System.Drawing.Size(80, 17);
            this.label_CerealStored.TabIndex = 7;
            this.label_CerealStored.Text = "Cer. Stored";
            this.label_CerealStored.Click += new System.EventHandler(this.label_CerealStored_Click);
            // 
            // label_MaxVegLimit
            // 
            this.label_MaxVegLimit.AutoSize = true;
            this.label_MaxVegLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MaxVegLimit.Location = new System.Drawing.Point(233, 45);
            this.label_MaxVegLimit.Name = "label_MaxVegLimit";
            this.label_MaxVegLimit.Size = new System.Drawing.Size(66, 17);
            this.label_MaxVegLimit.TabIndex = 8;
            this.label_MaxVegLimit.Text = "Veg Limit";
            this.label_MaxVegLimit.Click += new System.EventHandler(this.label_MaxVegLimit_Click);
            // 
            // label_MaxFruitLimit
            // 
            this.label_MaxFruitLimit.AutoSize = true;
            this.label_MaxFruitLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MaxFruitLimit.Location = new System.Drawing.Point(233, 78);
            this.label_MaxFruitLimit.Name = "label_MaxFruitLimit";
            this.label_MaxFruitLimit.Size = new System.Drawing.Size(69, 17);
            this.label_MaxFruitLimit.TabIndex = 9;
            this.label_MaxFruitLimit.Text = "Fruit Limit";
            this.label_MaxFruitLimit.Click += new System.EventHandler(this.label_MaxFruitLimit_Click);
            // 
            // label_MaxCerealLimit
            // 
            this.label_MaxCerealLimit.AutoSize = true;
            this.label_MaxCerealLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MaxCerealLimit.Location = new System.Drawing.Point(233, 113);
            this.label_MaxCerealLimit.Name = "label_MaxCerealLimit";
            this.label_MaxCerealLimit.Size = new System.Drawing.Size(82, 17);
            this.label_MaxCerealLimit.TabIndex = 10;
            this.label_MaxCerealLimit.Text = "Cereal Limit";
            this.label_MaxCerealLimit.Click += new System.EventHandler(this.label_MaxCerealLimit_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(224, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "/";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(224, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "/";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(224, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 17);
            this.label8.TabIndex = 13;
            this.label8.Text = "/";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label_LimitInfo
            // 
            this.label_LimitInfo.AutoSize = true;
            this.label_LimitInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_LimitInfo.Location = new System.Drawing.Point(247, 9);
            this.label_LimitInfo.Name = "label_LimitInfo";
            this.label_LimitInfo.Size = new System.Drawing.Size(42, 17);
            this.label_LimitInfo.TabIndex = 16;
            this.label_LimitInfo.Text = "Limit";
            // 
            // label_StoredCurrentInfo
            // 
            this.label_StoredCurrentInfo.AutoSize = true;
            this.label_StoredCurrentInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_StoredCurrentInfo.Location = new System.Drawing.Point(162, 9);
            this.label_StoredCurrentInfo.Name = "label_StoredCurrentInfo";
            this.label_StoredCurrentInfo.Size = new System.Drawing.Size(56, 17);
            this.label_StoredCurrentInfo.TabIndex = 15;
            this.label_StoredCurrentInfo.Text = "Stored";
            // 
            // label_StorageType
            // 
            this.label_StorageType.AutoSize = true;
            this.label_StorageType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_StorageType.Location = new System.Drawing.Point(24, 9);
            this.label_StorageType.Name = "label_StorageType";
            this.label_StorageType.Size = new System.Drawing.Size(106, 17);
            this.label_StorageType.TabIndex = 14;
            this.label_StorageType.Text = "Storage Type";
            this.label_StorageType.Click += new System.EventHandler(this.label4_Click);
            // 
            // Storage_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 209);
            this.Controls.Add(this.label_LimitInfo);
            this.Controls.Add(this.label_StoredCurrentInfo);
            this.Controls.Add(this.label_StorageType);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_MaxCerealLimit);
            this.Controls.Add(this.label_MaxFruitLimit);
            this.Controls.Add(this.label_MaxVegLimit);
            this.Controls.Add(this.label_CerealStored);
            this.Controls.Add(this.label_FruitStored);
            this.Controls.Add(this.label_VegStored);
            this.Controls.Add(this.button_buyStorage);
            this.Controls.Add(this.label_cerealStorage);
            this.Controls.Add(this.label_fruitStorage);
            this.Controls.Add(this.label_vegStorage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Storage_Menu";
            this.Text = "Storage";
            this.Load += new System.EventHandler(this.Storage_Menu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_vegStorage;
        private System.Windows.Forms.Label label_fruitStorage;
        private System.Windows.Forms.Label label_cerealStorage;
        private System.Windows.Forms.Button button_buyStorage;
        private System.Windows.Forms.Label label_VegStored;
        private System.Windows.Forms.Label label_FruitStored;
        private System.Windows.Forms.Label label_CerealStored;
        private System.Windows.Forms.Label label_MaxVegLimit;
        private System.Windows.Forms.Label label_MaxFruitLimit;
        private System.Windows.Forms.Label label_MaxCerealLimit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label_LimitInfo;
        private System.Windows.Forms.Label label_StoredCurrentInfo;
        private System.Windows.Forms.Label label_StorageType;
    }
}