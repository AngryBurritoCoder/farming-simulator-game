﻿namespace Farm_simulator
{
    partial class Stats_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stats_Menu));
            this.label_vegAvgQuality = new System.Windows.Forms.Label();
            this.label_fruitAvgQuality = new System.Windows.Forms.Label();
            this.label_cerealAvgQuality = new System.Windows.Forms.Label();
            this.label_profit = new System.Windows.Forms.Label();
            this.label_VegAvgQual_Display = new System.Windows.Forms.Label();
            this.label_FruitAvgQual_Display = new System.Windows.Forms.Label();
            this.label_CerealAvgQual_Display = new System.Windows.Forms.Label();
            this.label_Profit_Display = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_vegAvgQuality
            // 
            this.label_vegAvgQuality.AutoSize = true;
            this.label_vegAvgQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_vegAvgQuality.Location = new System.Drawing.Point(12, 21);
            this.label_vegAvgQuality.Name = "label_vegAvgQuality";
            this.label_vegAvgQuality.Size = new System.Drawing.Size(201, 20);
            this.label_vegAvgQuality.TabIndex = 0;
            this.label_vegAvgQuality.Text = "Vegetable Average Quality:";
            this.label_vegAvgQuality.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_fruitAvgQuality
            // 
            this.label_fruitAvgQuality.AutoSize = true;
            this.label_fruitAvgQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fruitAvgQuality.Location = new System.Drawing.Point(13, 52);
            this.label_fruitAvgQuality.Name = "label_fruitAvgQuality";
            this.label_fruitAvgQuality.Size = new System.Drawing.Size(160, 20);
            this.label_fruitAvgQuality.TabIndex = 1;
            this.label_fruitAvgQuality.Text = "Fruit Average Quality:";
            // 
            // label_cerealAvgQuality
            // 
            this.label_cerealAvgQuality.AutoSize = true;
            this.label_cerealAvgQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cerealAvgQuality.Location = new System.Drawing.Point(12, 83);
            this.label_cerealAvgQuality.Name = "label_cerealAvgQuality";
            this.label_cerealAvgQuality.Size = new System.Drawing.Size(174, 20);
            this.label_cerealAvgQuality.TabIndex = 2;
            this.label_cerealAvgQuality.Text = "Cereal Average Quality:";
            this.label_cerealAvgQuality.Click += new System.EventHandler(this.label2_Click);
            // 
            // label_profit
            // 
            this.label_profit.AutoSize = true;
            this.label_profit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_profit.Location = new System.Drawing.Point(13, 119);
            this.label_profit.Name = "label_profit";
            this.label_profit.Size = new System.Drawing.Size(50, 20);
            this.label_profit.TabIndex = 3;
            this.label_profit.Text = "Profit:";
            // 
            // label_VegAvgQual_Display
            // 
            this.label_VegAvgQual_Display.AutoSize = true;
            this.label_VegAvgQual_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_VegAvgQual_Display.Location = new System.Drawing.Point(219, 21);
            this.label_VegAvgQual_Display.Name = "label_VegAvgQual_Display";
            this.label_VegAvgQual_Display.Size = new System.Drawing.Size(19, 20);
            this.label_VegAvgQual_Display.TabIndex = 27;
            this.label_VegAvgQual_Display.Text = "_";
            // 
            // label_FruitAvgQual_Display
            // 
            this.label_FruitAvgQual_Display.AutoSize = true;
            this.label_FruitAvgQual_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FruitAvgQual_Display.Location = new System.Drawing.Point(179, 52);
            this.label_FruitAvgQual_Display.Name = "label_FruitAvgQual_Display";
            this.label_FruitAvgQual_Display.Size = new System.Drawing.Size(19, 20);
            this.label_FruitAvgQual_Display.TabIndex = 28;
            this.label_FruitAvgQual_Display.Text = "_";
            // 
            // label_CerealAvgQual_Display
            // 
            this.label_CerealAvgQual_Display.AutoSize = true;
            this.label_CerealAvgQual_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CerealAvgQual_Display.Location = new System.Drawing.Point(192, 83);
            this.label_CerealAvgQual_Display.Name = "label_CerealAvgQual_Display";
            this.label_CerealAvgQual_Display.Size = new System.Drawing.Size(19, 20);
            this.label_CerealAvgQual_Display.TabIndex = 29;
            this.label_CerealAvgQual_Display.Text = "_";
            this.label_CerealAvgQual_Display.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // label_Profit_Display
            // 
            this.label_Profit_Display.AutoSize = true;
            this.label_Profit_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Profit_Display.Location = new System.Drawing.Point(69, 119);
            this.label_Profit_Display.Name = "label_Profit_Display";
            this.label_Profit_Display.Size = new System.Drawing.Size(19, 20);
            this.label_Profit_Display.TabIndex = 30;
            this.label_Profit_Display.Text = "_";
            // 
            // Stats_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 207);
            this.Controls.Add(this.label_Profit_Display);
            this.Controls.Add(this.label_CerealAvgQual_Display);
            this.Controls.Add(this.label_FruitAvgQual_Display);
            this.Controls.Add(this.label_VegAvgQual_Display);
            this.Controls.Add(this.label_profit);
            this.Controls.Add(this.label_cerealAvgQuality);
            this.Controls.Add(this.label_fruitAvgQuality);
            this.Controls.Add(this.label_vegAvgQuality);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Stats_Menu";
            this.Text = "Statistics";
            this.Load += new System.EventHandler(this.Stats_Menu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_vegAvgQuality;
        private System.Windows.Forms.Label label_fruitAvgQuality;
        private System.Windows.Forms.Label label_cerealAvgQuality;
        private System.Windows.Forms.Label label_profit;
        private System.Windows.Forms.Label label_VegAvgQual_Display;
        private System.Windows.Forms.Label label_FruitAvgQual_Display;
        private System.Windows.Forms.Label label_CerealAvgQual_Display;
        private System.Windows.Forms.Label label_Profit_Display;
    }
}