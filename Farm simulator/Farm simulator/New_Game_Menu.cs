﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class New_Game_Menu : Form
    {
        private Player playerClass = new Player();
        private Start_Menu closeStartMenu = new Start_Menu();

        public New_Game_Menu()
        {
            InitializeComponent();
        }

        private void button__Cancel_NewGame_Click(object sender, EventArgs e)
        {
            //new Start_Menu().Show();
            this.Hide();
        }

        private void button_Start_NewGame_Click(object sender, EventArgs e)
        {
            new Main_Menu().ShowDialog();
            closeStartMenu.CloseThis();
            new Stats().ResetQualities();
            this.Close();
        }

        private void textBox_PlayerName_TextChanged(object sender, EventArgs e)
        {
            playerClass.PlayerName = textBox_PlayerName.Text;
        }

        private void textBox_FarmName_TextChanged(object sender, EventArgs e)
        {
            playerClass.FarmName = textBox_FarmName.Text;
        }
    }
}
