﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    class Field
    {
        //Variables
        private bool isFieldCropFullyGrown;
        private bool isFieldEmpty;
        private bool isFieldFertilised;
        private bool canFertiliseField;
        private bool isFieldInfected;
        private string fieldStatus;
        private float fieldGrowthProcess;
        private string fieldCropType;
        private float fieldCropQuality;
        private float cerealGrowthRate = 1.8f;
        private float vegGrowthRate = 1.2f;
        private float fruitGrowthRate = 0.8f;

        private Weather weatherClass = new Weather();
        private SelectedField selectedFieldClass = new SelectedField();
        private Stats statsClass = new Stats();
        private Storage storageClass = new Storage(0,0,"noCrop");


        public Field(bool isFieldGrown, bool isFieldEmpty, bool isFieldFertilised, bool canFertiliseField, bool isFieldInfected,
            string fieldStatus, float fieldGrowthProcess, string fieldCropType, float fieldCropQuality)
        {
            this.isFieldCropFullyGrown = isFieldGrown;
            this.isFieldEmpty = isFieldEmpty;
            this.isFieldFertilised = isFieldFertilised;
            this.canFertiliseField = canFertiliseField;
            this.isFieldInfected = isFieldInfected;
            this.fieldStatus = fieldStatus;
            this.fieldGrowthProcess = fieldGrowthProcess;
            this.fieldCropType = fieldCropType;
            this.fieldCropQuality = fieldCropQuality;
        }

        public Field()
        {

        }

        //Field list
        public static List<Field> fields = new List<Field>();

        #region Encapsulations of private variables
        public bool IsFieldCropFullyGrown
        {
            get { return isFieldCropFullyGrown; }
            set { isFieldCropFullyGrown = value; }
        }

        public bool IsFieldEmpty
        {
            get { return isFieldEmpty; }
            set { isFieldEmpty = value; }
        }

        public bool IsFieldFertilised
        {
            get { return isFieldFertilised; }
            set { isFieldFertilised = value; }
        }

        public bool CanFertiliseField
        {
            get { return canFertiliseField; }
            set { canFertiliseField = value; }
        }

        public bool IsFieldInfected
        {
            get { return isFieldInfected; }
            set { isFieldInfected = value; }
        }

        public string FieldStatus
        {
            get { return fieldStatus; }
            set { fieldStatus = value; }
        }

        public float FieldGrowthProcess
        {
            get { return fieldGrowthProcess; }
            set { fieldGrowthProcess = value; }
        }

        public string FieldCropType
        {
            get { return fieldCropType; }
            set { fieldCropType = value; }
        }

        public float FieldCropQuality
        {
            get { return fieldCropQuality; }
            set { fieldCropQuality = value; }
        }

        public float CerealGrowthRate
        {
            get { return cerealGrowthRate; }
            set { cerealGrowthRate = value; }
        }

        public float VegGrowthRate
        {
            get { return vegGrowthRate; }
            set { vegGrowthRate = value; }
        }

        public float FruitGrowthRate
        {
            get { return fruitGrowthRate; }
            set { fruitGrowthRate = value; }
        }
        #endregion the

        //Adds new field to the list of fields
        public void BuyField(int quantity)
        {
            
            for (int index = 0; index < quantity; index++)
            {
                fields.Add(new Field(false, true, false, true, false, "None", 0f, "Empty Field", 0f));
            }
            
        }
        
        //Fertilise the field method
        public void FertiliseField()
        {
            //Fertilise selected field
            fields[selectedFieldClass.FieldID].isFieldFertilised = true;
            statsClass.AddSpecialItem("Fertiliser", -1);
        }

        //Treat the field for infection
        public void TreatField()
        {
            //Treat selected field
            fields[selectedFieldClass.FieldID].isFieldInfected = false;
            statsClass.AddSpecialItem("Pesticide", -1);
        }

        //Plant on the field
        public void PlantField(string cropType)
        {
            //Plant on selected field
            fields[selectedFieldClass.FieldID].isFieldEmpty = false;
            fields[selectedFieldClass.FieldID].FieldCropType = cropType;
            fields[selectedFieldClass.FieldID].fieldCropQuality = 10f;
            fields[selectedFieldClass.FieldID].fieldGrowthProcess = 0f;

            statsClass.AddSeed(cropType, -1);
        }

        //Harvest the field when crops fully grown
        public void HarvestField()
        {
            string thisFieldCropType = fields[selectedFieldClass.FieldID].fieldCropType;
            float thisFieldQualityAmount = fields[selectedFieldClass.FieldID].fieldCropQuality;

            storageClass.AddToStorage(thisFieldCropType);
            statsClass.UpdateQualitiesYearly(thisFieldCropType, thisFieldQualityAmount);

            //Harvest selected field, and reset all the fields variables to default
            fields[selectedFieldClass.FieldID].isFieldEmpty = true;
            fields[selectedFieldClass.FieldID].isFieldFertilised = false;
            fields[selectedFieldClass.FieldID].isFieldCropFullyGrown = false;
            fields[selectedFieldClass.FieldID].canFertiliseField = true;
            fields[selectedFieldClass.FieldID].fieldCropQuality = 0f;
            fields[selectedFieldClass.FieldID].fieldCropType = "None";
            fields[selectedFieldClass.FieldID].fieldGrowthProcess = 0f;
            fields[selectedFieldClass.FieldID].fieldStatus = "Empty Field";
            fields[selectedFieldClass.FieldID].isFieldInfected = false;

            //COUNT NEW stored QUALITY AVERAGES
        }

        //Destroy the crops on the field
        public void DestroyFieldCrops()
        {
            //Reset field variables
            fields[selectedFieldClass.FieldID].isFieldEmpty = true;
            fields[selectedFieldClass.FieldID].isFieldFertilised = false;
            fields[selectedFieldClass.FieldID].isFieldCropFullyGrown = false;
            fields[selectedFieldClass.FieldID].canFertiliseField = true;
            fields[selectedFieldClass.FieldID].fieldCropQuality = 0f;
            fields[selectedFieldClass.FieldID].fieldCropType = "None";
            fields[selectedFieldClass.FieldID].fieldGrowthProcess = 0f;
            fields[selectedFieldClass.FieldID].fieldStatus = "Empty Field";
            fields[selectedFieldClass.FieldID].isFieldInfected = false;

        }

        public void UpdateFields()
        {
            
            #region Summary

            /*- Get current weather
                                 * - Based on weather ( Switch(weatherType)) apply a buff/nerf to the Growth of crops
                                 * - Based on weather, randomly generate and see if any special event has been applied to the field (Flooding, Draught,Heavy winds)
                                 * - based on special event and crop type, lower the Quality/Growth of crops
                                 * 
                                 *- Get field Treatment/Fertiliser/Infection status
                                 * - If field is fertilised, increase quality of crop by preset amount each turn
                                 * - If field is not treated, generate a random chance of infection to the crops
                                 * - if field is infected, decrease quality of crops each turn, and decrease overall growth rate by preset amounts
                                 * 
                                 *- Get Field Crop Status
                                 * - If field crops fully grown ( crop number = 10 ), decrease quality each turn by preset amount
                                 * - If field isnt fully grown, increase the crop number by adjusted growth rate based by other variables from before, check if 10 is hit.
                                 * - Check the quality of crop, if is below or = 0, destroy crop automatically, if above 10, make quality = 10
                                 * - Warn player of any extreme changes, like field being destroyed, or available for harvesting,or if is infected 
                                 */
            #endregion

            //Check what field status will be ( random )
            if (weatherClass.WeatherType != "Calm")
            {
                int eventNumber;

                Random random = new Random();

                switch (weatherClass.WeatherType)
                {
                    case "Raining":

                        #region If Raining
                        foreach (Field element in fields)
                        {
                            //If isFieldEmpty != true && isFieldCropFullyGrown != true, then field can be modified
                            if (!element.isFieldEmpty && !element.isFieldCropFullyGrown)
                            {
                                eventNumber = random.Next(0, 100);

                                if (eventNumber <= 60)
                                {
                                    element.fieldStatus = "Flooding";
                                }
                                else
                                {
                                    element.fieldStatus = "Crop Growing";
                                }
                            }
                        }
                        #endregion

                        break;
                    case "Snowing":

                        #region If Snowing
                        foreach (Field element in fields)
                        {
                            //If isFieldEmpty != true && isFieldCropFullyGrown != true, then field can be modified
                            if (!element.isFieldEmpty && !element.isFieldCropFullyGrown)
                            {
                                eventNumber = random.Next(0, 100);

                                if (eventNumber <= 45)
                                {
                                    element.fieldStatus = "Crop Freezing";
                                }
                                else
                                {
                                    element.fieldStatus = "Crop Growing";
                                }
                            }
                        }
                        #endregion

                        break;
                    case "Strong Winds":

                        #region If Strong Winds
                        foreach (Field element in fields)
                        {
                            //If isFieldEmpty != true && isFieldCropFullyGrown != true, then field can be modified
                            if (!element.isFieldEmpty && !element.isFieldCropFullyGrown)
                            {
                                eventNumber = random.Next(0, 100);

                                if (eventNumber <= 20)
                                {
                                    element.fieldStatus = "Hurricane";
                                }
                                else
                                {
                                    element.fieldStatus = "Crop Growing";
                                }
                            }
                        }
                        #endregion

                        break;
                    case "Sunny":

                        #region If Hot
                        foreach (Field element in fields)
                        {
                            //If isFieldEmpty != true && isFieldCropFullyGrown != true, then field can be modified
                            if (!element.isFieldEmpty && !element.isFieldCropFullyGrown)
                            {
                                eventNumber = random.Next(0, 100);

                                if (eventNumber <= 50)
                                {
                                    element.fieldStatus = "Drought";
                                }
                                else
                                {
                                    element.fieldStatus = "Crop Growing";
                                }
                            }
                        }
                        #endregion

                        break;
                }
            }

            //Check if field got infected ( random )
            foreach (Field element in fields)
            {
                if (!element.isFieldEmpty && !element.isFieldCropFullyGrown && !element.isFieldInfected)
                {
                    int infectionNumber;

                    Random random = new Random();
                    infectionNumber = random.Next(0, 100);

                    if (infectionNumber <= 20)
                    {
                        element.isFieldInfected = true;
                    }
                }
            }

            //Progress the crop along towards full growth using factors of the field
            foreach (Field element in fields)
            {
                if (!element.isFieldEmpty && !element.isFieldCropFullyGrown)
                {
                    //Increase growth process of crop based on crop type
                    switch (element.fieldCropType)
                    {
                        case "Cereal":
                            element.fieldGrowthProcess += cerealGrowthRate;

                            //Update Growth Proccess by nornmal weather
                            UpdateGrowthByWeather(element, 0f, -0.1f, 0f, 0.2f, 0f);

                            //Update Growth Proccess by special weather events + infection + fertilisation
                            UpdateGrowthProccessBySpecialEvents(element, 0.4f, 0.6f, 0.4f, 1f);

                            //Update crop quality
                            UpdateCropQuality(element, 0.8f, 1f, 0.9f, 1.6f);
                            break;
                        case "Fruit":
                            element.fieldGrowthProcess += fruitGrowthRate;

                            //Update Growth Proccess by nornmal weather
                            UpdateGrowthByWeather(element, 0.2f, 0f, -0.1f, 0.2f, 0f);

                            //Update Growth Proccess by special weather events + infection + fertilisation
                            UpdateGrowthProccessBySpecialEvents(element, 0.2f, 0.4f, 0.7f, 0.6f);

                            //Update crop quality
                            UpdateCropQuality(element, 0.5f, 1.2f, 1.7f, 1.6f);
                            break;
                        case "Veg":
                            element.fieldGrowthProcess += vegGrowthRate;

                            //Update Growth Proccess by nornmal weather
                            UpdateGrowthByWeather(element, 0.1f, -0.2f, 0f,0.1f, 0f);

                            //Update Growth Proccess by special weather events + infection + fertilisation
                            UpdateGrowthProccessBySpecialEvents(element,0.6f,0.4f,0.4f,0.5f);

                            //Update crop quality
                            UpdateCropQuality(element,0.8f,0.8f,1f,1.6f);
                            break;
                    }

                    //if field growth proccess reaches 10, stop growing and allow harvesting
                    if (element.fieldGrowthProcess >= 10)
                    {
                        element.fieldGrowthProcess = 10;
                        element.isFieldCropFullyGrown = true;
                        element.fieldStatus = "Ready To Harvest";
                        element.isFieldInfected = false;
                    }

                    //if field crop quality reaches 10 or above, set quality at 10 max
                    if (element.fieldCropQuality >= 10)
                    {
                        element.fieldCropQuality = 10;
                    }
                }
            }
        }

        //Lowers the growth proccess based on the current special events in weather, if fertilised and if infected
        public void UpdateGrowthProccessBySpecialEvents(Field element,float f,float cF,float d,float h)
        {
            //Change growth proccess based on field status and crop type
            switch (element.fieldStatus)
            {
                case "Flooding":
                    element.fieldGrowthProcess -= f;
                    break;
                case "Crop Freezing":
                    element.fieldGrowthProcess -= cF;
                    break;
                case "Drought":
                    element.fieldGrowthProcess -= d;
                    break;
                case "Hurricane":
                    element.fieldGrowthProcess -= h;
                    break;
                case "Crop Growing":
                    break;
            }

            //if field is fertilised, increase growth proccess by specified amount
            if (element.isFieldFertilised)
            {
                element.fieldGrowthProcess += 0.2f;
            }

            //if field is infected, reduce growth proccess by specified amount
            if (element.isFieldInfected)
            {
                element.fieldGrowthProcess -= 0.3f;
            }

        }

        //Update crop growth proccess depending on the normal weather and crop type
        public void UpdateGrowthByWeather(Field element,float r,float s,float sW,float sN,float c)
        {
            //If field is not experiencing any special weather events, then allow for growth proccess to be changed by normal weather
            if(element.fieldStatus == "Crop Growing")
            { 
                #region Change growth and quality based on weather type ( Not special events )
                switch (weatherClass.WeatherType)
                {
                    case "Raining":
                        element.fieldGrowthProcess += r;
                        break;
                    case "Snowing":
                        element.fieldGrowthProcess += s;
                        break;
                    case "Strong Winds":
                        element.fieldGrowthProcess += sW;
                        break;
                    case "Sunny":
                        element.fieldGrowthProcess += sN;
                        break;
                    case "Calm":
                        element.fieldGrowthProcess += c;
                        break;
                }
                #endregion
            }
        }

        //Quality of crop update
        public void UpdateCropQuality(Field element,float f,float cF,float d,float h)
        {
            //reduce/increase quality based on weather event
            switch (element.fieldStatus)
            {
                case "Flooding":
                    element.fieldCropQuality -= f;
                    break;
                case "Crop Freezing":
                    element.fieldCropQuality -= cF;
                    break;
                case "Drought":
                    element.fieldCropQuality -= d;
                    break;
                case "Hurricane":
                    element.fieldCropQuality -= h;
                    break;
                case "Crop Growing":
                    element.fieldCropQuality += 0.1f;
                    break;
            }

            //If fertilised increase quality of crop every turn
            if (element.isFieldFertilised)
            {
                element.fieldCropQuality += 0.1f;
            }
             
            //if field is infected , reduce quality every turn
            if (element.isFieldInfected)
            {
                element.fieldCropQuality -= 0.3f;
            }

        }
    }
}
