﻿namespace Farm_simulator
{
    partial class Market_menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Market_menu));
            this.tabControlMarket = new System.Windows.Forms.TabControl();
            this.tabPgSell = new System.Windows.Forms.TabPage();
            this.numSellQuantity2 = new System.Windows.Forms.NumericUpDown();
            this.lblPrice2 = new System.Windows.Forms.Label();
            this.lblPriceText = new System.Windows.Forms.Label();
            this.lblStoredQuantity2 = new System.Windows.Forms.Label();
            this.lblStoredText = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lstBoxCrops = new System.Windows.Forms.ListBox();
            this.btnSell = new System.Windows.Forms.Button();
            this.tabPgBuy = new System.Windows.Forms.TabPage();
            this.lblBuyPrice2 = new System.Windows.Forms.Label();
            this.lblBuyPriceText = new System.Windows.Forms.Label();
            this.numBuyQuantity = new System.Windows.Forms.NumericUpDown();
            this.lblBuyQuantity2 = new System.Windows.Forms.Label();
            this.btnBuy = new System.Windows.Forms.Button();
            this.lblBuyQuantityText = new System.Windows.Forms.Label();
            this.lstBoxBuy = new System.Windows.Forms.ListBox();
            this.tabControlMarket.SuspendLayout();
            this.tabPgSell.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSellQuantity2)).BeginInit();
            this.tabPgBuy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBuyQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlMarket
            // 
            this.tabControlMarket.Controls.Add(this.tabPgSell);
            this.tabControlMarket.Controls.Add(this.tabPgBuy);
            this.tabControlMarket.Location = new System.Drawing.Point(12, 12);
            this.tabControlMarket.Name = "tabControlMarket";
            this.tabControlMarket.SelectedIndex = 0;
            this.tabControlMarket.Size = new System.Drawing.Size(448, 263);
            this.tabControlMarket.TabIndex = 0;
            // 
            // tabPgSell
            // 
            this.tabPgSell.Controls.Add(this.numSellQuantity2);
            this.tabPgSell.Controls.Add(this.lblPrice2);
            this.tabPgSell.Controls.Add(this.lblPriceText);
            this.tabPgSell.Controls.Add(this.lblStoredQuantity2);
            this.tabPgSell.Controls.Add(this.lblStoredText);
            this.tabPgSell.Controls.Add(this.lblQuantity);
            this.tabPgSell.Controls.Add(this.lstBoxCrops);
            this.tabPgSell.Controls.Add(this.btnSell);
            this.tabPgSell.Location = new System.Drawing.Point(4, 22);
            this.tabPgSell.Name = "tabPgSell";
            this.tabPgSell.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgSell.Size = new System.Drawing.Size(440, 237);
            this.tabPgSell.TabIndex = 0;
            this.tabPgSell.Text = "Sell";
            this.tabPgSell.UseVisualStyleBackColor = true;
            this.tabPgSell.Click += new System.EventHandler(this.tabPgSell_Click);
            // 
            // numSellQuantity2
            // 
            this.numSellQuantity2.Location = new System.Drawing.Point(253, 86);
            this.numSellQuantity2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numSellQuantity2.Name = "numSellQuantity2";
            this.numSellQuantity2.Size = new System.Drawing.Size(116, 20);
            this.numSellQuantity2.TabIndex = 8;
            this.numSellQuantity2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSellQuantity2.ValueChanged += new System.EventHandler(this.numSellQuantity2_ValueChanged);
            this.numSellQuantity2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numSellQuantity2_KeyUp);
            // 
            // lblPrice2
            // 
            this.lblPrice2.AutoSize = true;
            this.lblPrice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPrice2.Location = new System.Drawing.Point(232, 114);
            this.lblPrice2.Name = "lblPrice2";
            this.lblPrice2.Size = new System.Drawing.Size(0, 17);
            this.lblPrice2.TabIndex = 7;
            // 
            // lblPriceText
            // 
            this.lblPriceText.AutoSize = true;
            this.lblPriceText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPriceText.Location = new System.Drawing.Point(182, 114);
            this.lblPriceText.Name = "lblPriceText";
            this.lblPriceText.Size = new System.Drawing.Size(44, 17);
            this.lblPriceText.TabIndex = 6;
            this.lblPriceText.Text = "Price:";
            // 
            // lblStoredQuantity2
            // 
            this.lblStoredQuantity2.AutoSize = true;
            this.lblStoredQuantity2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblStoredQuantity2.Location = new System.Drawing.Point(299, 57);
            this.lblStoredQuantity2.Name = "lblStoredQuantity2";
            this.lblStoredQuantity2.Size = new System.Drawing.Size(0, 17);
            this.lblStoredQuantity2.TabIndex = 5;
            this.lblStoredQuantity2.Click += new System.EventHandler(this.lblStoredQuantity2_Click);
            // 
            // lblStoredText
            // 
            this.lblStoredText.AutoSize = true;
            this.lblStoredText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblStoredText.Location = new System.Drawing.Point(182, 57);
            this.lblStoredText.Name = "lblStoredText";
            this.lblStoredText.Size = new System.Drawing.Size(111, 17);
            this.lblStoredText.TabIndex = 4;
            this.lblStoredText.Text = "Stored Quantity:";
            this.lblStoredText.Click += new System.EventHandler(this.lblStoredText_Click);
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblQuantity.Location = new System.Drawing.Point(182, 86);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(65, 17);
            this.lblQuantity.TabIndex = 2;
            this.lblQuantity.Text = "Quantity:";
            // 
            // lstBoxCrops
            // 
            this.lstBoxCrops.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lstBoxCrops.FormattingEnabled = true;
            this.lstBoxCrops.ItemHeight = 16;
            this.lstBoxCrops.Items.AddRange(new object[] {
            "Vegetable",
            "Fruit",
            "Cereal"});
            this.lstBoxCrops.Location = new System.Drawing.Point(17, 23);
            this.lstBoxCrops.Name = "lstBoxCrops";
            this.lstBoxCrops.Size = new System.Drawing.Size(136, 180);
            this.lstBoxCrops.TabIndex = 1;
            this.lstBoxCrops.SelectedIndexChanged += new System.EventHandler(this.lstBoxCrops_SelectedIndexChanged);
            // 
            // btnSell
            // 
            this.btnSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSell.Location = new System.Drawing.Point(225, 158);
            this.btnSell.Name = "btnSell";
            this.btnSell.Size = new System.Drawing.Size(144, 45);
            this.btnSell.TabIndex = 0;
            this.btnSell.Text = "Sell";
            this.btnSell.UseVisualStyleBackColor = true;
            this.btnSell.Click += new System.EventHandler(this.btnSell_Click);
            // 
            // tabPgBuy
            // 
            this.tabPgBuy.Controls.Add(this.lblBuyPrice2);
            this.tabPgBuy.Controls.Add(this.lblBuyPriceText);
            this.tabPgBuy.Controls.Add(this.numBuyQuantity);
            this.tabPgBuy.Controls.Add(this.lblBuyQuantity2);
            this.tabPgBuy.Controls.Add(this.btnBuy);
            this.tabPgBuy.Controls.Add(this.lblBuyQuantityText);
            this.tabPgBuy.Controls.Add(this.lstBoxBuy);
            this.tabPgBuy.Location = new System.Drawing.Point(4, 22);
            this.tabPgBuy.Name = "tabPgBuy";
            this.tabPgBuy.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgBuy.Size = new System.Drawing.Size(440, 237);
            this.tabPgBuy.TabIndex = 1;
            this.tabPgBuy.Text = "Buy";
            this.tabPgBuy.UseVisualStyleBackColor = true;
            // 
            // lblBuyPrice2
            // 
            this.lblBuyPrice2.AutoSize = true;
            this.lblBuyPrice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBuyPrice2.Location = new System.Drawing.Point(232, 114);
            this.lblBuyPrice2.Name = "lblBuyPrice2";
            this.lblBuyPrice2.Size = new System.Drawing.Size(0, 17);
            this.lblBuyPrice2.TabIndex = 7;
            // 
            // lblBuyPriceText
            // 
            this.lblBuyPriceText.AutoSize = true;
            this.lblBuyPriceText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBuyPriceText.Location = new System.Drawing.Point(182, 114);
            this.lblBuyPriceText.Name = "lblBuyPriceText";
            this.lblBuyPriceText.Size = new System.Drawing.Size(44, 17);
            this.lblBuyPriceText.TabIndex = 6;
            this.lblBuyPriceText.Text = "Price:";
            // 
            // numBuyQuantity
            // 
            this.numBuyQuantity.Location = new System.Drawing.Point(253, 86);
            this.numBuyQuantity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numBuyQuantity.Name = "numBuyQuantity";
            this.numBuyQuantity.Size = new System.Drawing.Size(116, 20);
            this.numBuyQuantity.TabIndex = 5;
            this.numBuyQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numBuyQuantity.ValueChanged += new System.EventHandler(this.numBuyQuantity_ValueChanged);
            this.numBuyQuantity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numBuyQuantity_KeyUp);
            // 
            // lblBuyQuantity2
            // 
            this.lblBuyQuantity2.AutoSize = true;
            this.lblBuyQuantity2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBuyQuantity2.Location = new System.Drawing.Point(253, 86);
            this.lblBuyQuantity2.Name = "lblBuyQuantity2";
            this.lblBuyQuantity2.Size = new System.Drawing.Size(0, 17);
            this.lblBuyQuantity2.TabIndex = 3;
            // 
            // btnBuy
            // 
            this.btnBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnBuy.Location = new System.Drawing.Point(225, 158);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(144, 45);
            this.btnBuy.TabIndex = 2;
            this.btnBuy.Text = "Buy";
            this.btnBuy.UseVisualStyleBackColor = true;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // lblBuyQuantityText
            // 
            this.lblBuyQuantityText.AutoSize = true;
            this.lblBuyQuantityText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBuyQuantityText.Location = new System.Drawing.Point(182, 86);
            this.lblBuyQuantityText.Name = "lblBuyQuantityText";
            this.lblBuyQuantityText.Size = new System.Drawing.Size(65, 17);
            this.lblBuyQuantityText.TabIndex = 1;
            this.lblBuyQuantityText.Text = "Quantity:";
            // 
            // lstBoxBuy
            // 
            this.lstBoxBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lstBoxBuy.FormattingEnabled = true;
            this.lstBoxBuy.ItemHeight = 16;
            this.lstBoxBuy.Items.AddRange(new object[] {
            "Vegeteable Seed",
            "Fruit Seed",
            "Cereal Seed",
            "Fertiliser",
            "Pesticide",
            "New Field"});
            this.lstBoxBuy.Location = new System.Drawing.Point(17, 23);
            this.lstBoxBuy.Name = "lstBoxBuy";
            this.lstBoxBuy.Size = new System.Drawing.Size(136, 180);
            this.lstBoxBuy.TabIndex = 0;
            this.lstBoxBuy.SelectedIndexChanged += new System.EventHandler(this.lstBoxBuy_SelectedIndexChanged);
            // 
            // Market_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 287);
            this.Controls.Add(this.tabControlMarket);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Market_menu";
            this.Text = "Market";
            this.Load += new System.EventHandler(this.Market_menu_Load);
            this.tabControlMarket.ResumeLayout(false);
            this.tabPgSell.ResumeLayout(false);
            this.tabPgSell.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSellQuantity2)).EndInit();
            this.tabPgBuy.ResumeLayout(false);
            this.tabPgBuy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBuyQuantity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMarket;
        private System.Windows.Forms.TabPage tabPgSell;
        private System.Windows.Forms.TabPage tabPgBuy;
        private System.Windows.Forms.ListBox lstBoxCrops;
        private System.Windows.Forms.Button btnSell;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label lblStoredQuantity2;
        private System.Windows.Forms.Label lblStoredText;
        private System.Windows.Forms.Label lblBuyQuantity2;
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.Label lblBuyQuantityText;
        private System.Windows.Forms.ListBox lstBoxBuy;
        private System.Windows.Forms.Label lblPrice2;
        private System.Windows.Forms.Label lblPriceText;
        private System.Windows.Forms.NumericUpDown numBuyQuantity;
        private System.Windows.Forms.NumericUpDown numSellQuantity2;
        private System.Windows.Forms.Label lblBuyPrice2;
        private System.Windows.Forms.Label lblBuyPriceText;
    }
}