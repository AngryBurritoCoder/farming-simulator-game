﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_simulator
{
    class Bank
    {
        #region variables
        private static float currentBalance = 10000f;        
        private float interestRate = 0.5f;
        private float interestRateBalance;
        private static float loan = 0f;
        private static bool isLoanTaken = false;
        private float debtInterestAmount = 0.5f;
        private Stats stats = new Stats();

        public float DebtInterestAmount
        {
            get { return debtInterestAmount; }
            set { debtInterestAmount = value; }
        }  
        public bool IsLoanTaken
        {
            get { return Bank.isLoanTaken; }
            set { Bank.isLoanTaken = value; }
        } 




        //encapsulation
        public float CurrentBalance
        {
            get { return Bank.currentBalance; }
            set { Bank.currentBalance = value; }
        }
        public float InterestRate
        {
            get { return interestRate; }
            set { interestRate = value; }
        }
        public float InterestRateBalance
        {
            get { return interestRateBalance; }
            set { interestRateBalance = value; }
        }
        public float Loan
        {
            get { return Bank.loan; }
            set { Bank.loan = value; }
        }

        #endregion
        #region methods
        public void ApplyInterestRate()  
        {
            interestRateBalance = currentBalance / 100;
            stats.UpdateRevenue(interestRateBalance * interestRate);
            currentBalance = currentBalance + (interestRateBalance * interestRate);
            
        }   
        public void TakeLoan(float amount)
        {
            currentBalance = currentBalance + amount;
            loan = amount;
            isLoanTaken = true;    

        }      

        public void ApplyDebtInterest()
        {
            debtInterestAmount = loan / 100;
            loan = loan + (debtInterestAmount * 2.5f);
        }    
        public void PayLoan()
        {
            currentBalance = currentBalance - loan;
            stats.UpdateMoneySpent(loan);
            loan = 0;
            isLoanTaken = false; 


        }
        #endregion

    }
}
