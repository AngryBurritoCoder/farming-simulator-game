﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Set up default fields...
            #region Elements added to existent lists

            Field.fields.Add(new Field(false, true, false, true, false, "Normal", 0f, "Empty", 0f));
            Field.fields.Add(new Field(false, true, false, true, false, "Normal", 0f, "Empty", 0f));

           //Set up default storages
            Storage.storage.Add(new Storage(500,0,"Veg"));
            Storage.storage.Add(new Storage(500,0,"Fruit"));
            Storage.storage.Add(new Storage(500,0, "Cereal"));


            #endregion


            //Debug MessageBox...
            //MessageBox.Show(field.fields[2].FieldCropType);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Start_Menu());
        }

    }
}
