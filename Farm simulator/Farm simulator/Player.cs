﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_simulator
{
    class Player
    {

        private static string playerName;
        private static string farmName;

        public string PlayerName
        {
            get { return Player.playerName; }
            set { Player.playerName = value; }
        }

        public string FarmName
        {
            get { return Player.farmName; }
            set { Player.farmName = value; }
        }
    }
}
