﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Market_menu : Form
    {
        private Market market;
        private Storage storageObject;
        private Bank bank;
        private Stats stats;
        private Main_Menu mainMenu;
        private Field field;
        private float SellPrice;
        private float BuyPrice;

        //constructor
        public Market_menu(int tabSettings, Main_Menu mainMenu)
        {
            
            InitializeComponent();
            tabControlMarket.SelectTab(tabSettings);
            market = new Market();
            storageObject = new Storage();
            bank = new Bank();
            stats = new Stats();
            field = new Field();
            this.mainMenu = mainMenu;
            lstBoxCrops.SelectedIndex = 0;
            lstBoxBuy.SelectedIndex = 0;
        }
        public Market_menu(int tabSettings, Main_Menu mainMenu, int lstSettings)
        {

            InitializeComponent();
            tabControlMarket.SelectTab(tabSettings);
            lstBoxBuy.SelectedItem = lstSettings;
            market = new Market();
            storageObject = new Storage();
            bank = new Bank();
            stats = new Stats();
            field = new Field();
            this.mainMenu = mainMenu;
            lstBoxCrops.SelectedIndex = 0;
            lstBoxBuy.SelectedIndex = 0;
        }

        private void Market_menu_Load(object sender, EventArgs e)
        {

        }

        private void lstBoxCrops_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateText();
            
        }

        public void UpdateText()
        {
            //Sell Tab Texts
            lblStoredQuantity2.Text = Storage.storage[lstBoxCrops.SelectedIndex].StorageCurrent.ToString();
            switch (lstBoxCrops.SelectedIndex)
            {
                case 0:
                    lblPrice2.Text = Calculate(market.VegSellPrice, Convert.ToInt32(numSellQuantity2.Value)).ToString("c");
                    SellPrice = Calculate(market.VegSellPrice, Convert.ToInt32(numSellQuantity2.Value));
                    break;
                case 1:
                    lblPrice2.Text = Calculate(market.FruitSellPrice, Convert.ToInt32(numSellQuantity2.Value)).ToString("c");
                    SellPrice = Calculate(market.FruitSellPrice, Convert.ToInt32(numSellQuantity2.Value));
                    break;
                case 2:
                    lblPrice2.Text = Calculate(market.CerealSellPrice, Convert.ToInt32(numSellQuantity2.Value)).ToString("c");
                    SellPrice = Calculate(market.CerealSellPrice, Convert.ToInt32(numSellQuantity2.Value));
                    break;
            }

            //Buy Tab Texts
            switch (lstBoxBuy.SelectedIndex)
            { 
                case 0:
                    lblBuyPrice2.Text = Calculate(market.VegSeedPrice, Convert.ToInt32(numBuyQuantity.Value)).ToString("c");
                    BuyPrice = Calculate(market.VegSeedPrice, Convert.ToInt32(numBuyQuantity.Value));
                    break;
                case 1:
                    lblBuyPrice2.Text = Calculate(market.FruitSeedPrice, Convert.ToInt32(numBuyQuantity.Value)).ToString("c");
                    BuyPrice = Calculate(market.FruitSeedPrice, Convert.ToInt32(numBuyQuantity.Value));
                    break;
                case 2:
                    lblBuyPrice2.Text = Calculate(market.CerealSeedPrice, Convert.ToInt32(numBuyQuantity.Value)).ToString("c");
                    BuyPrice = Calculate(market.CerealSeedPrice, Convert.ToInt32(numBuyQuantity.Value));
                    break;
                case 3:
                    lblBuyPrice2.Text = Calculate(market.FertiliserPrice, Convert.ToInt32(numBuyQuantity.Value)).ToString("c");
                    BuyPrice = Calculate(market.FertiliserPrice, Convert.ToInt32(numBuyQuantity.Value));
                    break;
                case 4:
                    lblBuyPrice2.Text = Calculate(market.PesticidePrice, Convert.ToInt32(numBuyQuantity.Value)).ToString("c");
                    BuyPrice = Calculate(market.PesticidePrice, Convert.ToInt32(numBuyQuantity.Value));
                    break;
                case 5:
                    lblBuyPrice2.Text = Calculate(market.FieldPrice , Convert.ToInt32(numBuyQuantity.Value)).ToString("c");
                    BuyPrice = Calculate(market.FieldPrice, Convert.ToInt32(numBuyQuantity.Value));
                    break;
            }
            //update main menu
            
        }
        public float Calculate(float price, int quantity)
        {
            float result;
            result = price * quantity;
            return result;
        }


        private void btnBuy_Click(object sender, EventArgs e)
        {
            UpdateText();
            
            if (BuyPrice <= bank.CurrentBalance) //check if have sufficient money on the bank
            {
                //add to stats
                switch (lstBoxBuy.SelectedIndex)
                {
                    case 0:
                        stats.AddSeed("Veg", Convert.ToInt32(numBuyQuantity.Value));
                        break;
                    case 1:
                        stats.AddSeed("Fruit", Convert.ToInt32(numBuyQuantity.Value));
                        break;
                    case 2:
                        stats.AddSeed("Cereal", Convert.ToInt32(numBuyQuantity.Value));
                        break;
                    case 3:
                        stats.AddSpecialItem("Fertiliser", Convert.ToInt32(numBuyQuantity.Value));
                        break;
                    case 4:
                        stats.AddSpecialItem("Pesticide", Convert.ToInt32(numBuyQuantity.Value));
                        break;
                    case 5:
                        if ((Field.fields.Count) < 6)//this if statement will make sure that the player will not be able to buy more fields than the set limit
                        {
                            if ((Field.fields.Count + numBuyQuantity.Value) <= 6)
                            {
                                field.BuyField(Convert.ToInt32(numBuyQuantity.Value));
                            }
                            else
                            {
                                numBuyQuantity.Value = 6 - Field.fields.Count;
                                field.BuyField(Convert.ToInt32(numBuyQuantity.Value));
                                
                            }
                            
                        }
                        else
                        {
                            MessageBox.Show("You already have the maximum number of fields");
                            return;
                        }
                        break;
                }
                bank.CurrentBalance -= BuyPrice;
                stats.UpdateMoneySpent(BuyPrice);
                //deduct money from bank
            }
            else
            {
                MessageBox.Show("Not Enough Money");
            }
            UpdateText();
            mainMenu.UpdateLabels();
            
        }

        private void btnSell_Click(object sender, EventArgs e)
        {
            UpdateText();
            //check if the player has the right amount of item in storage
            if (Storage.storage[lstBoxCrops.SelectedIndex].StorageCurrent >= Convert.ToInt32(numSellQuantity2.Value))
            {
                if (lstBoxCrops.Text == "Vegetable")
                {
                    storageObject.RemoveFromStorage(Convert.ToInt32(numSellQuantity2.Value), "Veg");
                    //storageObject.AddToStorage()
                }
                else
                {

                    storageObject.RemoveFromStorage(Convert.ToInt32(numSellQuantity2.Value), lstBoxCrops.Text);
                }
                bank.CurrentBalance += SellPrice;
                stats.UpdateRevenue(SellPrice);
                //add money to bank
                //
            }
            UpdateText();
            mainMenu.UpdateLabels();
            
            
        }

        private void lblStoredText_Click(object sender, EventArgs e)
        {

        }

        private void lblStoredQuantity2_Click(object sender, EventArgs e)
        {

        }

        

        private void numBuyQuantity_ValueChanged(object sender, EventArgs e)
        {
            UpdateText();
        }

        private void tabPgSell_Click(object sender, EventArgs e)
        {

        }

        private void lstBoxBuy_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateText();
        }

        private void numSellQuantity2_ValueChanged(object sender, EventArgs e)
        {
            UpdateText();
        }

        private void numSellQuantity2_KeyUp(object sender, KeyEventArgs e)
        {
            UpdateText();
        }

        private void numBuyQuantity_KeyUp(object sender, KeyEventArgs e)
        {
            UpdateText();
        }
    }
}
