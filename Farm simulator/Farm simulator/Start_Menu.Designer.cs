﻿namespace Farm_simulator
{
    partial class Start_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Start_Menu));
            this.PictureBox_StartMenu = new System.Windows.Forms.PictureBox();
            this.button_NewGame = new System.Windows.Forms.Button();
            this.button_LoadGame = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_StartMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBox_StartMenu
            // 
            this.PictureBox_StartMenu.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox_StartMenu.Image")));
            this.PictureBox_StartMenu.Location = new System.Drawing.Point(12, 12);
            this.PictureBox_StartMenu.Name = "PictureBox_StartMenu";
            this.PictureBox_StartMenu.Size = new System.Drawing.Size(339, 137);
            this.PictureBox_StartMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_StartMenu.TabIndex = 0;
            this.PictureBox_StartMenu.TabStop = false;
            this.PictureBox_StartMenu.Click += new System.EventHandler(this.PictureBox_StartMenu_Click);
            // 
            // button_NewGame
            // 
            this.button_NewGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_NewGame.Location = new System.Drawing.Point(12, 167);
            this.button_NewGame.Name = "button_NewGame";
            this.button_NewGame.Size = new System.Drawing.Size(339, 69);
            this.button_NewGame.TabIndex = 1;
            this.button_NewGame.Text = "New Game";
            this.button_NewGame.UseVisualStyleBackColor = true;
            this.button_NewGame.Click += new System.EventHandler(this.button_NewGame_Click);
            // 
            // button_LoadGame
            // 
            this.button_LoadGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_LoadGame.Location = new System.Drawing.Point(12, 242);
            this.button_LoadGame.Name = "button_LoadGame";
            this.button_LoadGame.Size = new System.Drawing.Size(339, 69);
            this.button_LoadGame.TabIndex = 2;
            this.button_LoadGame.Text = "Load Game";
            this.button_LoadGame.UseVisualStyleBackColor = true;
            // 
            // Start_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 324);
            this.Controls.Add(this.button_LoadGame);
            this.Controls.Add(this.button_NewGame);
            this.Controls.Add(this.PictureBox_StartMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "Start_Menu";
            this.Text = "Farm Simulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_StartMenu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox_StartMenu;
        private System.Windows.Forms.Button button_NewGame;
        private System.Windows.Forms.Button button_LoadGame;
    }
}

