﻿namespace Farm_simulator
{
    partial class End_Game_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(End_Game_Menu));
            this.label_PlayerName_End = new System.Windows.Forms.Label();
            this.label_PlayerName_End_Display = new System.Windows.Forms.Label();
            this.label_FarmName_End = new System.Windows.Forms.Label();
            this.label_FarmName_End_Display = new System.Windows.Forms.Label();
            this.lblProfit2 = new System.Windows.Forms.Label();
            this.label_profit = new System.Windows.Forms.Label();
            this.label_cerealAvgQuality = new System.Windows.Forms.Label();
            this.label_fruitAvgQuality = new System.Windows.Forms.Label();
            this.label_vegAvgQuality = new System.Windows.Forms.Label();
            this.label_VegAvgQual_End_Display = new System.Windows.Forms.Label();
            this.label_FruitAvgQual_End_Display = new System.Windows.Forms.Label();
            this.label_FruitCerealQual_End_Display = new System.Windows.Forms.Label();
            this.label_Profit_End_Display = new System.Windows.Forms.Label();
            this.button_quitGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_PlayerName_End
            // 
            this.label_PlayerName_End.AutoSize = true;
            this.label_PlayerName_End.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PlayerName_End.Location = new System.Drawing.Point(12, 20);
            this.label_PlayerName_End.Name = "label_PlayerName_End";
            this.label_PlayerName_End.Size = new System.Drawing.Size(114, 20);
            this.label_PlayerName_End.TabIndex = 17;
            this.label_PlayerName_End.Text = "Player Name:";
            this.label_PlayerName_End.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_PlayerName_End_Display
            // 
            this.label_PlayerName_End_Display.AutoSize = true;
            this.label_PlayerName_End_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PlayerName_End_Display.Location = new System.Drawing.Point(132, 20);
            this.label_PlayerName_End_Display.Name = "label_PlayerName_End_Display";
            this.label_PlayerName_End_Display.Size = new System.Drawing.Size(19, 20);
            this.label_PlayerName_End_Display.TabIndex = 18;
            this.label_PlayerName_End_Display.Text = "_";
            this.label_PlayerName_End_Display.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_FarmName_End
            // 
            this.label_FarmName_End.AutoSize = true;
            this.label_FarmName_End.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FarmName_End.Location = new System.Drawing.Point(319, 20);
            this.label_FarmName_End.Name = "label_FarmName_End";
            this.label_FarmName_End.Size = new System.Drawing.Size(106, 20);
            this.label_FarmName_End.TabIndex = 19;
            this.label_FarmName_End.Text = "Farm Name:";
            this.label_FarmName_End.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_FarmName_End.Click += new System.EventHandler(this.label_FarmName_End_Click);
            // 
            // label_FarmName_End_Display
            // 
            this.label_FarmName_End_Display.AutoSize = true;
            this.label_FarmName_End_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FarmName_End_Display.Location = new System.Drawing.Point(431, 20);
            this.label_FarmName_End_Display.Name = "label_FarmName_End_Display";
            this.label_FarmName_End_Display.Size = new System.Drawing.Size(19, 20);
            this.label_FarmName_End_Display.TabIndex = 20;
            this.label_FarmName_End_Display.Text = "_";
            this.label_FarmName_End_Display.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProfit2
            // 
            this.lblProfit2.AutoSize = true;
            this.lblProfit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfit2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.lblProfit2.Location = new System.Drawing.Point(184, 178);
            this.lblProfit2.Name = "lblProfit2";
            this.lblProfit2.Size = new System.Drawing.Size(0, 20);
            this.lblProfit2.TabIndex = 25;
            // 
            // label_profit
            // 
            this.label_profit.AutoSize = true;
            this.label_profit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_profit.Location = new System.Drawing.Point(258, 178);
            this.label_profit.Name = "label_profit";
            this.label_profit.Size = new System.Drawing.Size(57, 20);
            this.label_profit.TabIndex = 24;
            this.label_profit.Text = "Profit:";
            // 
            // label_cerealAvgQuality
            // 
            this.label_cerealAvgQuality.AutoSize = true;
            this.label_cerealAvgQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cerealAvgQuality.Location = new System.Drawing.Point(118, 147);
            this.label_cerealAvgQuality.Name = "label_cerealAvgQuality";
            this.label_cerealAvgQuality.Size = new System.Drawing.Size(197, 20);
            this.label_cerealAvgQuality.TabIndex = 23;
            this.label_cerealAvgQuality.Text = "Cereal Average Quality:";
            // 
            // label_fruitAvgQuality
            // 
            this.label_fruitAvgQuality.AutoSize = true;
            this.label_fruitAvgQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fruitAvgQuality.Location = new System.Drawing.Point(133, 116);
            this.label_fruitAvgQuality.Name = "label_fruitAvgQuality";
            this.label_fruitAvgQuality.Size = new System.Drawing.Size(182, 20);
            this.label_fruitAvgQuality.TabIndex = 22;
            this.label_fruitAvgQuality.Text = "Fruit Average Quality:";
            // 
            // label_vegAvgQuality
            // 
            this.label_vegAvgQuality.AutoSize = true;
            this.label_vegAvgQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_vegAvgQuality.Location = new System.Drawing.Point(88, 87);
            this.label_vegAvgQuality.Name = "label_vegAvgQuality";
            this.label_vegAvgQuality.Size = new System.Drawing.Size(227, 20);
            this.label_vegAvgQuality.TabIndex = 21;
            this.label_vegAvgQuality.Text = "Vegetable Average Quality:";
            // 
            // label_VegAvgQual_End_Display
            // 
            this.label_VegAvgQual_End_Display.AutoSize = true;
            this.label_VegAvgQual_End_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_VegAvgQual_End_Display.Location = new System.Drawing.Point(321, 87);
            this.label_VegAvgQual_End_Display.Name = "label_VegAvgQual_End_Display";
            this.label_VegAvgQual_End_Display.Size = new System.Drawing.Size(19, 20);
            this.label_VegAvgQual_End_Display.TabIndex = 26;
            this.label_VegAvgQual_End_Display.Text = "_";
            this.label_VegAvgQual_End_Display.Click += new System.EventHandler(this.label_VegAvgQual_End_Display_Click);
            // 
            // label_FruitAvgQual_End_Display
            // 
            this.label_FruitAvgQual_End_Display.AutoSize = true;
            this.label_FruitAvgQual_End_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FruitAvgQual_End_Display.Location = new System.Drawing.Point(321, 116);
            this.label_FruitAvgQual_End_Display.Name = "label_FruitAvgQual_End_Display";
            this.label_FruitAvgQual_End_Display.Size = new System.Drawing.Size(19, 20);
            this.label_FruitAvgQual_End_Display.TabIndex = 27;
            this.label_FruitAvgQual_End_Display.Text = "_";
            // 
            // label_FruitCerealQual_End_Display
            // 
            this.label_FruitCerealQual_End_Display.AutoSize = true;
            this.label_FruitCerealQual_End_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FruitCerealQual_End_Display.Location = new System.Drawing.Point(321, 147);
            this.label_FruitCerealQual_End_Display.Name = "label_FruitCerealQual_End_Display";
            this.label_FruitCerealQual_End_Display.Size = new System.Drawing.Size(19, 20);
            this.label_FruitCerealQual_End_Display.TabIndex = 28;
            this.label_FruitCerealQual_End_Display.Text = "_";
            // 
            // label_Profit_End_Display
            // 
            this.label_Profit_End_Display.AutoSize = true;
            this.label_Profit_End_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Profit_End_Display.Location = new System.Drawing.Point(321, 178);
            this.label_Profit_End_Display.Name = "label_Profit_End_Display";
            this.label_Profit_End_Display.Size = new System.Drawing.Size(19, 20);
            this.label_Profit_End_Display.TabIndex = 29;
            this.label_Profit_End_Display.Text = "_";
            // 
            // button_quitGame
            // 
            this.button_quitGame.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_quitGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_quitGame.Location = new System.Drawing.Point(16, 246);
            this.button_quitGame.Name = "button_quitGame";
            this.button_quitGame.Size = new System.Drawing.Size(548, 77);
            this.button_quitGame.TabIndex = 30;
            this.button_quitGame.Text = "Quit Game";
            this.button_quitGame.UseVisualStyleBackColor = true;
            this.button_quitGame.Click += new System.EventHandler(this.button_finishGame_Click);
            // 
            // End_Game_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button_quitGame;
            this.ClientSize = new System.Drawing.Size(576, 349);
            this.Controls.Add(this.button_quitGame);
            this.Controls.Add(this.label_Profit_End_Display);
            this.Controls.Add(this.label_FruitCerealQual_End_Display);
            this.Controls.Add(this.label_FruitAvgQual_End_Display);
            this.Controls.Add(this.label_VegAvgQual_End_Display);
            this.Controls.Add(this.lblProfit2);
            this.Controls.Add(this.label_profit);
            this.Controls.Add(this.label_cerealAvgQuality);
            this.Controls.Add(this.label_fruitAvgQuality);
            this.Controls.Add(this.label_vegAvgQuality);
            this.Controls.Add(this.label_FarmName_End_Display);
            this.Controls.Add(this.label_FarmName_End);
            this.Controls.Add(this.label_PlayerName_End_Display);
            this.Controls.Add(this.label_PlayerName_End);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "End_Game_Menu";
            this.Text = "Game Over";
            this.Load += new System.EventHandler(this.End_Game_Menu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_PlayerName_End;
        private System.Windows.Forms.Label label_PlayerName_End_Display;
        private System.Windows.Forms.Label label_FarmName_End;
        private System.Windows.Forms.Label label_FarmName_End_Display;
        private System.Windows.Forms.Label lblProfit2;
        private System.Windows.Forms.Label label_profit;
        private System.Windows.Forms.Label label_cerealAvgQuality;
        private System.Windows.Forms.Label label_fruitAvgQuality;
        private System.Windows.Forms.Label label_vegAvgQuality;
        private System.Windows.Forms.Label label_VegAvgQual_End_Display;
        private System.Windows.Forms.Label label_FruitAvgQual_End_Display;
        private System.Windows.Forms.Label label_FruitCerealQual_End_Display;
        private System.Windows.Forms.Label label_Profit_End_Display;
        private System.Windows.Forms.Button button_quitGame;

    }
}