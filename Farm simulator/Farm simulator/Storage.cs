﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    class Storage
    {
        public int storageLimit;
        private int storageCurrent;
        private string cropType;
        private int cropAmount;
        private static float storagePrice = 5000f;

        public Storage(int storageLimit, int storageCurrent, string cropType)
        {
            this.storageLimit = storageLimit;
            this.storageCurrent = storageCurrent;
            this.cropType = cropType;
        }
        public Storage()
        { 
            
        }

        private Bank bankClass = new Bank();

        public static List<Storage> storage = new List<Storage>();

        #region Encapsulation
        public int StorageLimit
        {
            get { return storageLimit; }
            set { storageLimit = value; }
        }

        public int StorageCurrent
        {
            get { return storageCurrent; }
            set { storageCurrent = value; }
        }

        public string CropType
        {
            get { return cropType; }
            set { cropType = value; }
        }


        public int CropAmount
        {
            get { return cropAmount; }
            set { cropAmount = value; }
        }

        public float StoragePrice
        {
            get { return storagePrice; }
            set { storagePrice = value; }
        }
        #endregion

        public void AddToStorage (string cropType)
        {

            switch(cropType)
            {
                case "Veg":
                    Storage.storage[0].storageCurrent += 250;

                    if (Storage.storage[0].storageCurrent > Storage.storage[0].storageLimit)
                    {
                        Storage.storage[0].storageCurrent = Storage.storage[0].storageLimit;
                    }
                    break;

                case "Fruit":
                    Storage.storage[1].storageCurrent += 250;

                    if (Storage.storage[1].storageCurrent > Storage.storage[1].storageLimit)
                    {
                        Storage.storage[1].storageCurrent = Storage.storage[1].storageLimit;
                    }
                    break;

                case "Cereal":
                    Storage.storage[2].storageCurrent += 250;

                    if(Storage.storage[2].storageCurrent > Storage.storage[2].storageLimit)
                    {
                        Storage.storage[2].storageCurrent = Storage.storage[2].storageLimit;
                    }
                    break;
            }
        }

        public void RemoveFromStorage (int cropAmount, string cropType)
        {
            switch (cropType)
            {
                case "Veg":

                    if(Storage.storage[0].storageCurrent >= cropAmount)
                    {
                        Storage.storage[0].storageCurrent -= cropAmount;
                    }

                    else
                    {
                        MessageBox.Show("The amount chosen is too high, there are not enough crops stored.");
                    }

                    break;

                case "Fruit":

                    if (Storage.storage[1].storageCurrent >= cropAmount)
                    {
                        Storage.storage[1].storageCurrent -= cropAmount;
                    }

                    else
                    {
                        MessageBox.Show("The amount chosen is too high, there are not enough crops stored.");
                    }

                    break;

                case "Cereal":

                    if (Storage.storage[2].storageCurrent >= cropAmount)
                    {
                        Storage.storage[2].storageCurrent -= cropAmount;
                    }

                    else
                    {
                        MessageBox.Show("The amount chosen is too high, there are not enough crops stored.");
                    }

                    break;
            }
        }

        //adds more storage space
        public void BuyStorage (string cropType)
        {
            switch(cropType)
            {
                case "Veg":
                    Storage.storage[0].storageLimit += 250;
                    bankClass.CurrentBalance -= storagePrice;
                    break;

                case "Fruit":
                    Storage.storage[1].storageLimit += 250;
                    bankClass.CurrentBalance -= storagePrice;
                    break;

                case "Cereal":
                    Storage.storage[2].storageLimit += 250;
                    bankClass.CurrentBalance -= storagePrice;
                    break;
            }
        }
    }
}
