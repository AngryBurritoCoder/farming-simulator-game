﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_simulator
{
    class NextTurn
    {

        private static int currentTurn = 0;
        private static int currentWeek = 0;
        private int weekIncrease = 52;
        private int yearIncrease = 1;
        private int weekCounter = 0;
        private int seasonCounter = 0;
        private static int currentYear = 0;
        private static string currentSeason = "Spring";

        #region Encapsulation
        public int CurrentTurn
        {
            get { return NextTurn.currentTurn; }
            set { NextTurn.currentTurn = value; }
        }

        public int CurrentWeek
        {
            get { return NextTurn.currentWeek; }
            set { NextTurn.currentWeek = value; }
        }

        public int WeekIncrease
        {
            get { return weekIncrease; }
            set { weekIncrease = value; }
        }

        public int YearIncrease
        {
            get { return yearIncrease; }
            set { yearIncrease = value; }
        }

        public int WeekCounter
        {
            get { return weekCounter; }
            set { weekCounter = value; }
        }

        public int SeasonCounter
        {
            get { return seasonCounter; }
            set { seasonCounter = value; }
        }

        public int CurrentYear
        {
            get { return NextTurn.currentYear; }
            set { NextTurn.currentYear = value; }
        }

        public string CurrentSeason
        {
            get { return NextTurn.currentSeason; }
            set { NextTurn.currentSeason = value; }
        }
        #endregion

        private Weather weatherClass = new Weather();
        private Stats statsClass = new Stats();
        private Market marketClass = new Market();
        private Field fieldClass = new Field(false,false,false,false,false,"none",0,"NoCrop",0);
        private Storage storageClass = new Storage(0,0,"noCrop");
        private Bank bankClass = new Bank();
        //private EndGame lastTurn = new EndGame();

        public void UpdateTurn()
        {
    
            #region Season/Week/Turn/Year Update

            currentTurn += 1;
            currentWeek += 1;
            weekCounter += 1;

            //Increase current year based on weeks passed
            if (currentWeek >= weekIncrease)
            {
                currentYear += 1;
                weekIncrease += 52;
            }

            //If week counter >= 13 ( seasons cycle every 13 weeks) make week counter = 0, and choose correct season
            if (weekCounter >= 13)
            {
                weekCounter = 0;
                seasonCounter += 1;

                //if season counter reaches 3, reset the seasons ( make season counter  = 0)
                if (seasonCounter >= 4)
                {
                    seasonCounter = 0;
                }

                switch (seasonCounter)
                {
                    case 0:
                        currentSeason = "Spring";
                        break;
                    case 1:
                        currentSeason = "Summer";
                        break;
                    case 2:
                        currentSeason = "Autumn";
                        break;
                    case 3:
                        currentSeason = "Winter";
                        break;
                }
            }
            #endregion

            #region Calling class methods to update everything

            weatherClass.UpdateWeather(currentSeason);
            marketClass.UpdatePrices();
            fieldClass.UpdateFields();
            bankClass.ApplyDebtInterest();
            bankClass.ApplyInterestRate();
            //lastTurn.EndGame();

            if (currentYear == yearIncrease)
            {
                statsClass.ResetQualities();
                yearIncrease += 1;
            }
            #endregion
        }

    }
}
