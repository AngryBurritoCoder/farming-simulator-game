﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_simulator
{
    class Weather
    {

        private static string weatherType = "Calm";


        #region encapsulation fields
        public string WeatherType
        {
            get { return weatherType; }
            set { weatherType = value; }
        }
        #endregion

        //Update the weather every turn
        public void UpdateWeather(string season)
        {
            Random weatherRandom = new Random();
            int weatherID = weatherRandom.Next(0, 100);

            //Switch to find correct current season
            switch (season)
            {
                    //Based on randomly generated number, decide the correct weather for the correct season
                case "Winter":

                    if (weatherID >= 0 && weatherID <= 40)
                    {
                        weatherType = "Snowing";
                    }
                    else if (weatherID > 40 && weatherID <= 80)
                    {
                        weatherType = "Calm";
                    }
                    else if (weatherID > 80 && weatherID <= 100)
                    {
                        weatherType = "Raining";
                    }

                    break;
                case "Spring":

                    if (weatherID >= 0 && weatherID <= 50)
                    {
                        weatherType = "Raining";
                    }
                    else if (weatherID > 50 && weatherID <= 80)
                    {
                        weatherType = "Calm";
                    }
                    else if (weatherID > 80 && weatherID <= 100)
                    {
                        weatherType = "Strong Winds";
                    }

                    break;
                case "Summer":

                    if (weatherID >= 0 && weatherID <= 35)
                    {
                        weatherType = "Sunny";
                    }
                    else if (weatherID > 35 && weatherID <= 85)
                    {
                        weatherType = "Calm";
                    }
                    else if (weatherID > 85 && weatherID <= 100)
                    {
                        weatherType = "Raining";
                    }

                    break;
                case "Autumn":

                    if (weatherID >= 0 && weatherID <= 45)
                    {
                        weatherType = "Raining";
                    }
                    else if (weatherID > 45 && weatherID <= 65)
                    {
                        weatherType = "Calm";
                    }
                    else if (weatherID > 65 && weatherID <= 100)
                    {
                        weatherType = "Strong Winds";
                    }

                    break;
            }
        }
    }
}
