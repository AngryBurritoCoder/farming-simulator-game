﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Created by Franz Pascual
//Version 1.0

namespace Farm_simulator
{
    class Stats
    {
        
        #region Attributes
        // I made the variables static so that you can create instance of it on multiple classes, whenever you add or subtract it will affect the original class. so basically we only have 1 stats instance/object
        private static float avgCerealQual;
        private static float avgFruitQuality;
        private static float avgVegQuality;
        private static float avgStoredCerealQual;
        private static float avgStoredFruitQual;
        private static float avgStoredVegQual;
        private static float overallProfit;
        private static float overallRevenue;
        private static float overallSpending;        
        private static int cerealSeed = 1;
        private static int vegSeed = 1;
        private static int fruitSeed = 1;
        private static int fertiliser = 1;
        private static int pesticide = 1;
                
        #endregion
        
        #region Encapsulation
        public float AvgCerealQual
        {
            get { return avgCerealQual; }
            set { avgCerealQual = value; }
        }
        public float AvgFruitQuality
        {
            get { return avgFruitQuality; }
            set { avgFruitQuality = value; }
        }
        public float AvgVegQuality
        {
            get { return avgVegQuality; }
            set { avgVegQuality = value; }
        }
        public float AvgStoredCerealQual
        {
            get { return avgStoredCerealQual; }
            set { avgStoredCerealQual = value; }
        }
        public float AvgStoredFruitQual
        {
            get { return avgStoredFruitQual; }
            set { avgStoredFruitQual = value; }
        }
        public float AvgStoredVegQual
        {
            get { return avgStoredVegQual; }
            set { avgStoredVegQual = value; }
        }
        public float OverallProfit
        {
            get { return overallProfit; }
            set { overallProfit = value; }
        }
        public float OverallRevenue
        {
            get { return overallRevenue; }
            set { overallRevenue = value; }
        }
        public float OverallSpending
        {
            get { return overallSpending; }
            set { overallSpending = value; }
        }
        public int CerealSeed
        {
            get { return cerealSeed; }
            set { cerealSeed = value; }
        }
        public int VegSeed
        {
            get { return vegSeed; }
            set { vegSeed = value; }
        }
        public int FruitSeed
        {
            get { return fruitSeed; }
            set { fruitSeed = value; }
        }
        public int Fertiliser
        {
            get { return fertiliser; }
            set { fertiliser = value; }
        }
        public int Pesticide
        {
            get { return pesticide; }
            set { pesticide = value; }
        }
        #endregion

        #region Method
        /// <summary>
        /// Updates the quality of the crops harvested on yearly basis
        /// </summary>
        /// <param name="cropType"></param>
        /// determine which crop quality to update (Please enter one of the following without quotation marks: "Fruit", "Veg" or "Cereal")
        /// <param name="cropQual"></param>
        /// Determine how much is the quality of the last harvested crop and updates/calculate it from the old value
        public void UpdateQualitiesYearly(string cropType, float cropQual)
        {
            switch (cropType)
            {
                case "Cereal":
                    if (avgCerealQual == 0)
                    {
                        avgCerealQual = cropQual;
                    }
                    else
                    {
                        avgCerealQual += cropQual;
                        avgCerealQual = avgCerealQual / 2;
                    }
                    break;
                case "Fruit":
                    if (avgFruitQuality == 0)
                    {
                        avgFruitQuality = cropQual;
                    }
                    else
                    {
                        avgFruitQuality += cropQual;
                        avgFruitQuality = avgFruitQuality / 2;
                    }
                    break;
                case "Veg":
                    if (avgVegQuality == 0)
                    {
                        avgVegQuality = cropQual;
                    }
                    else
                    {
                        avgVegQuality += cropQual;
                        avgVegQuality = avgVegQuality / 2;
                    }
                    break;
                default:
                    break;

            }

        }
        /// <summary>
        /// Updates the quality of harvested crop inside the storage
        /// </summary>
        /// <param name="cropType"></param>
        /// determine which crop quality to update (Please enter one of the following without quotation marks: "Fruit", "Veg" or "Cereal")
        /// <param name="cropQual"></param>
        /// Determine how much is the quality of the last stored crop and updates/calculate it from the old value 
        public void UpdateStorageQuality(string cropType, float cropQual)
        {
            
            switch (cropType)
            {
                case "Cereal":
                    if (Storage.storage[2].StorageCurrent <= 0)
                    {
                        avgStoredCerealQual = 0;                    
                    }
                    if (avgStoredCerealQual == 0)
                    {
                        avgStoredCerealQual = cropQual;
                    }
                    else
                    {
                        avgStoredCerealQual += cropQual;
                        avgStoredCerealQual = avgStoredCerealQual / 2;
                    }    
                    break;
                case "Fruit":
                    if (Storage.storage[1].StorageCurrent <= 0)
                    {
                        avgStoredFruitQual = 0;                    
                    }
                    if (avgStoredFruitQual == 0)
                    {
                        avgStoredFruitQual = cropQual;
                    }
                    else
                    {
                        avgStoredFruitQual += cropQual;
                        avgStoredFruitQual = avgStoredFruitQual / 2;
                    }    
                    break;
                case "Veg":
                    if (Storage.storage[0].StorageCurrent > 0)
                    {
                        avgStoredVegQual = 0;               
                    }
                    if (avgStoredVegQual == 0)
                    {
                        avgStoredVegQual = cropQual;
                    }
                    else
                    {
                        avgStoredVegQual += cropQual;
                        avgStoredVegQual = avgStoredVegQual / 2;
                    }
                    
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Resets the quality of the harvested crop to 0
        /// </summary>
        public void ResetQualities()
        {
            avgCerealQual = 0;
            avgFruitQuality = 0;
            avgVegQuality = 0;
        }
        /// <summary>
        /// Calculate the profit made by the player. Substract the overall spending from the revenue made
        /// </summary>
        public void UpdateProfit()
        {
            overallProfit = overallRevenue - overallSpending;
        }
        /// <summary>
        /// Adds the value to overall spending everytime the player spend their money
        /// </summary>
        /// <param name="amount"></param>
        /// Determines the value to add to the overall spending
        public void UpdateMoneySpent(float amount)
        {
            overallSpending += amount;
        }
        /// <summary>
        /// Adds the revenue made by the player by selling harvested crops to the overall revenue variable.
        /// </summary>
        /// <param name="amount"></param>
        /// Determines the value to add to the overall revenue
        public void UpdateRevenue(float amount)
        {
            overallRevenue += amount;
        }
        /// <summary>
        /// Add seed to the specific seed variable everytime the player buys them from the store
        /// </summary>
        /// <param name="seedType"></param>
        /// determine which seed to add (Please enter one of the following without quotation marks: "Fruit", "Veg" or "Cereal")
        /// <param name="amount"></param>
        /// determine how much value to add as int
        public void AddSeed(string seedType, int amount)
        {
            if (seedType == null || amount == 0)
                return;
            else
            {
                switch (seedType)
                {
                    case "Fruit":
                        fruitSeed += amount;
                        break;
                    case "Veg":
                        vegSeed += amount;
                        break;
                    case "Cereal":
                        cerealSeed += amount;
                        break;
                    default:
                        break;
                }
            }
            
        }
        /// <summary>
        /// Add special item i.e. fertiliser and pesticide to their specific variable everytime the player buys them from the store
        /// </summary>
        /// <param name="item"></param>
        /// determines which special item to add (Please enter one of the following without quotation marks: "Fertiliser" or "Pesticide")
        /// <param name="amount"></param>
        /// determines how much value to add as int
        public void AddSpecialItem(string item, int amount)
        {
            switch (item)
            {
                case "Fertiliser":
                    fertiliser += amount;
                    break;
                case "Pesticide":
                    pesticide += amount;
                    break;
                
                default:
                    break;
            }
        }
        #endregion


    }
}
