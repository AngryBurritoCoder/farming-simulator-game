﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class End_Game_Menu : Form
    {
        private Player playerInfo = new Player();
        private Stats statsEnd = new Stats();

        public End_Game_Menu()
        {
            InitializeComponent();
            UpdateLabelsEnd();
        }

        public void UpdateLabelsEnd()
        {
            label_PlayerName_End_Display.Text = playerInfo.PlayerName;
            label_FarmName_End_Display.Text = playerInfo.FarmName;
            label_VegAvgQual_End_Display.Text = statsEnd.AvgStoredVegQual.ToString();
            label_FruitAvgQual_End_Display.Text = statsEnd.AvgStoredFruitQual.ToString();
            label_FruitCerealQual_End_Display.Text = statsEnd.AvgStoredCerealQual.ToString();
        }

        private void label_VegAvgQual_End_Display_Click(object sender, EventArgs e)
        {

        }

        private void label_FarmName_End_Click(object sender, EventArgs e)
        {

        }

        private void button_finishGame_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void End_Game_Menu_Load(object sender, EventArgs e)
        {

        }
    }
}
