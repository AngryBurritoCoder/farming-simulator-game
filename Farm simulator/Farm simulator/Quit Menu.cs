﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Quit_Menu : Form
    {
        public Quit_Menu()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void button_Yes_Click(object sender, EventArgs e)
        {

        }

        private void button_CancelSave_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_No_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
