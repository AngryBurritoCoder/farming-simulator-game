﻿namespace Farm_simulator
{
    partial class Take_Loan_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Take_Loan_Menu));
            this.label_loanAmount = new System.Windows.Forms.Label();
            this.LoanTakeOut = new System.Windows.Forms.TextBox();
            this.label_interestRate = new System.Windows.Forms.Label();
            this.InterestRateOnLoan = new System.Windows.Forms.Label();
            this.button_takeLoan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_loanAmount
            // 
            this.label_loanAmount.AutoSize = true;
            this.label_loanAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_loanAmount.Location = new System.Drawing.Point(12, 45);
            this.label_loanAmount.Name = "label_loanAmount";
            this.label_loanAmount.Size = new System.Drawing.Size(151, 17);
            this.label_loanAmount.TabIndex = 0;
            this.label_loanAmount.Text = "Enter Desired Amount:";
            this.label_loanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LoanTakeOut
            // 
            this.LoanTakeOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoanTakeOut.Location = new System.Drawing.Point(169, 42);
            this.LoanTakeOut.Name = "LoanTakeOut";
            this.LoanTakeOut.Size = new System.Drawing.Size(94, 23);
            this.LoanTakeOut.TabIndex = 1;
            // 
            // label_interestRate
            // 
            this.label_interestRate.AutoSize = true;
            this.label_interestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_interestRate.Location = new System.Drawing.Point(70, 81);
            this.label_interestRate.Name = "label_interestRate";
            this.label_interestRate.Size = new System.Drawing.Size(93, 17);
            this.label_interestRate.TabIndex = 2;
            this.label_interestRate.Text = "Interest Rate:";
            this.label_interestRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InterestRateOnLoan
            // 
            this.InterestRateOnLoan.AutoSize = true;
            this.InterestRateOnLoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InterestRateOnLoan.Location = new System.Drawing.Point(166, 81);
            this.InterestRateOnLoan.Name = "InterestRateOnLoan";
            this.InterestRateOnLoan.Size = new System.Drawing.Size(17, 17);
            this.InterestRateOnLoan.TabIndex = 3;
            this.InterestRateOnLoan.Text = "_";
            this.InterestRateOnLoan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_takeLoan
            // 
            this.button_takeLoan.Location = new System.Drawing.Point(12, 133);
            this.button_takeLoan.Name = "button_takeLoan";
            this.button_takeLoan.Size = new System.Drawing.Size(251, 68);
            this.button_takeLoan.TabIndex = 4;
            this.button_takeLoan.Text = "Take Loan";
            this.button_takeLoan.UseVisualStyleBackColor = true;
            this.button_takeLoan.Click += new System.EventHandler(this.button_takeLoan_Click);
            // 
            // Take_Loan_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 213);
            this.Controls.Add(this.button_takeLoan);
            this.Controls.Add(this.InterestRateOnLoan);
            this.Controls.Add(this.label_interestRate);
            this.Controls.Add(this.LoanTakeOut);
            this.Controls.Add(this.label_loanAmount);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Take_Loan_Menu";
            this.Text = "Take Loan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_loanAmount;
        private System.Windows.Forms.TextBox LoanTakeOut;
        private System.Windows.Forms.Label label_interestRate;
        private System.Windows.Forms.Label InterestRateOnLoan;
        private System.Windows.Forms.Button button_takeLoan;
    }
}