﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_simulator
{
    class SelectedField
    {
        private static int fieldID;

        public int FieldID
        {
            get { return fieldID; }
            set { fieldID = value; }
        }

    }
}
