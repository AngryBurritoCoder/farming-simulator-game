﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Storage_Menu : Form
    {

        private Main_Menu menu;

        public Storage_Menu(Main_Menu menuForm)
        {
            InitializeComponent();
            this.menu = menuForm;
            UpdateText();
        }
        public Storage_Menu()
        {
            InitializeComponent();
            UpdateText();
        }

        public void UpdateText()
        {
            menu.UpdateLabels();
            label_CerealStored.Text = Storage.storage[2].StorageCurrent.ToString();
            label_MaxCerealLimit.Text = Storage.storage[2].StorageLimit.ToString();
            label_FruitStored.Text = Storage.storage[1].StorageCurrent.ToString();
            label_MaxFruitLimit.Text = Storage.storage[1].StorageLimit.ToString();
            label_VegStored.Text = Storage.storage[0].StorageCurrent.ToString();
            label_MaxVegLimit.Text = Storage.storage[0].StorageLimit.ToString();
        }

        private void button_buyStorage_Click(object sender, EventArgs e)
        {
            new Buy_Storage_Menu(this).ShowDialog();
        }

        private void Storage_Menu_Load(object sender, EventArgs e)
        {
            
        }

        private void label_VegStored_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label_MaxCerealLimit_Click(object sender, EventArgs e)
        {

        }

        private void label_MaxFruitLimit_Click(object sender, EventArgs e)
        {

        }

        private void label_MaxVegLimit_Click(object sender, EventArgs e)
        {

        }

        private void label_CerealStored_Click(object sender, EventArgs e)
        {

        }

        private void label_FruitStored_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
