﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Start_Menu : Form
    {
        public Start_Menu()
        {
            InitializeComponent();
        }

        public void CloseThis()
        {
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_NewGame_Click(object sender, EventArgs e)
        {
            new New_Game_Menu().ShowDialog();
            //this.Hide();
        }

        private void PictureBox_StartMenu_Click(object sender, EventArgs e)
        {

        }
    }
}
