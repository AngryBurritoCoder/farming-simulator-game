﻿namespace Farm_simulator
{
    partial class Quit_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Quit_Menu));
            this.label_saveGame = new System.Windows.Forms.Label();
            this.button_Yes = new System.Windows.Forms.Button();
            this.button_No = new System.Windows.Forms.Button();
            this.button_CancelSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_saveGame
            // 
            this.label_saveGame.AutoSize = true;
            this.label_saveGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_saveGame.Location = new System.Drawing.Point(101, 22);
            this.label_saveGame.Name = "label_saveGame";
            this.label_saveGame.Size = new System.Drawing.Size(90, 17);
            this.label_saveGame.TabIndex = 0;
            this.label_saveGame.Text = "Save Game?";
            this.label_saveGame.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_saveGame.Click += new System.EventHandler(this.label1_Click);
            // 
            // button_Yes
            // 
            this.button_Yes.Location = new System.Drawing.Point(12, 75);
            this.button_Yes.Name = "button_Yes";
            this.button_Yes.Size = new System.Drawing.Size(75, 23);
            this.button_Yes.TabIndex = 1;
            this.button_Yes.Text = "Yes";
            this.button_Yes.UseVisualStyleBackColor = true;
            this.button_Yes.Click += new System.EventHandler(this.button_Yes_Click);
            // 
            // button_No
            // 
            this.button_No.Location = new System.Drawing.Point(104, 75);
            this.button_No.Name = "button_No";
            this.button_No.Size = new System.Drawing.Size(75, 23);
            this.button_No.TabIndex = 2;
            this.button_No.Text = "No";
            this.button_No.UseVisualStyleBackColor = true;
            this.button_No.Click += new System.EventHandler(this.button_No_Click);
            // 
            // button_CancelSave
            // 
            this.button_CancelSave.Location = new System.Drawing.Point(197, 75);
            this.button_CancelSave.Name = "button_CancelSave";
            this.button_CancelSave.Size = new System.Drawing.Size(75, 23);
            this.button_CancelSave.TabIndex = 3;
            this.button_CancelSave.Text = "Cancel";
            this.button_CancelSave.UseVisualStyleBackColor = true;
            this.button_CancelSave.Click += new System.EventHandler(this.button_CancelSave_Click);
            // 
            // Quit_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 128);
            this.Controls.Add(this.button_CancelSave);
            this.Controls.Add(this.button_No);
            this.Controls.Add(this.button_Yes);
            this.Controls.Add(this.label_saveGame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Quit_Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_saveGame;
        private System.Windows.Forms.Button button_Yes;
        private System.Windows.Forms.Button button_No;
        private System.Windows.Forms.Button button_CancelSave;
    }
}