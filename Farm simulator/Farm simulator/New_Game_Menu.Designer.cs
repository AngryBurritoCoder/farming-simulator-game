﻿namespace Farm_simulator
{
    partial class New_Game_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(New_Game_Menu));
            this.label_PlayerName = new System.Windows.Forms.Label();
            this.textBox_PlayerName = new System.Windows.Forms.TextBox();
            this.label_FarmName = new System.Windows.Forms.Label();
            this.textBox_FarmName = new System.Windows.Forms.TextBox();
            this.button__Cancel_NewGame = new System.Windows.Forms.Button();
            this.button_Start_NewGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_PlayerName
            // 
            this.label_PlayerName.AutoSize = true;
            this.label_PlayerName.Location = new System.Drawing.Point(33, 43);
            this.label_PlayerName.Name = "label_PlayerName";
            this.label_PlayerName.Size = new System.Drawing.Size(74, 13);
            this.label_PlayerName.TabIndex = 0;
            this.label_PlayerName.Text = "Player\'s Name";
            // 
            // textBox_PlayerName
            // 
            this.textBox_PlayerName.Location = new System.Drawing.Point(113, 40);
            this.textBox_PlayerName.Name = "textBox_PlayerName";
            this.textBox_PlayerName.Size = new System.Drawing.Size(147, 20);
            this.textBox_PlayerName.TabIndex = 1;
            this.textBox_PlayerName.TextChanged += new System.EventHandler(this.textBox_PlayerName_TextChanged);
            // 
            // label_FarmName
            // 
            this.label_FarmName.AutoSize = true;
            this.label_FarmName.Location = new System.Drawing.Point(33, 87);
            this.label_FarmName.Name = "label_FarmName";
            this.label_FarmName.Size = new System.Drawing.Size(68, 13);
            this.label_FarmName.TabIndex = 2;
            this.label_FarmName.Text = "Farm\'s Name";
            // 
            // textBox_FarmName
            // 
            this.textBox_FarmName.Location = new System.Drawing.Point(113, 84);
            this.textBox_FarmName.Name = "textBox_FarmName";
            this.textBox_FarmName.Size = new System.Drawing.Size(147, 20);
            this.textBox_FarmName.TabIndex = 3;
            this.textBox_FarmName.TextChanged += new System.EventHandler(this.textBox_FarmName_TextChanged);
            // 
            // button__Cancel_NewGame
            // 
            this.button__Cancel_NewGame.Location = new System.Drawing.Point(36, 133);
            this.button__Cancel_NewGame.Name = "button__Cancel_NewGame";
            this.button__Cancel_NewGame.Size = new System.Drawing.Size(75, 23);
            this.button__Cancel_NewGame.TabIndex = 4;
            this.button__Cancel_NewGame.Text = "Cancel";
            this.button__Cancel_NewGame.UseVisualStyleBackColor = true;
            this.button__Cancel_NewGame.Click += new System.EventHandler(this.button__Cancel_NewGame_Click);
            // 
            // button_Start_NewGame
            // 
            this.button_Start_NewGame.Location = new System.Drawing.Point(185, 133);
            this.button_Start_NewGame.Name = "button_Start_NewGame";
            this.button_Start_NewGame.Size = new System.Drawing.Size(75, 23);
            this.button_Start_NewGame.TabIndex = 5;
            this.button_Start_NewGame.Text = "Start";
            this.button_Start_NewGame.UseVisualStyleBackColor = true;
            this.button_Start_NewGame.Click += new System.EventHandler(this.button_Start_NewGame_Click);
            // 
            // New_Game_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 187);
            this.Controls.Add(this.button_Start_NewGame);
            this.Controls.Add(this.button__Cancel_NewGame);
            this.Controls.Add(this.textBox_FarmName);
            this.Controls.Add(this.label_FarmName);
            this.Controls.Add(this.textBox_PlayerName);
            this.Controls.Add(this.label_PlayerName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "New_Game_Menu";
            this.Text = "Player Details";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_PlayerName;
        private System.Windows.Forms.TextBox textBox_PlayerName;
        private System.Windows.Forms.Label label_FarmName;
        private System.Windows.Forms.TextBox textBox_FarmName;
        private System.Windows.Forms.Button button__Cancel_NewGame;
        private System.Windows.Forms.Button button_Start_NewGame;
    }
}